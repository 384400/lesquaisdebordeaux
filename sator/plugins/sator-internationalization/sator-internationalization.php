<?php
/**
 * Plugin Name: Sator Internationalization
 * Plugin URI: http://www.384400.xyz
 * Description: A simple way to display translations with Javascript and Css. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

/**
 * Dans une nouvelle version, nous utiliserons les chams personnalises pour traduire le titre "en dur", c'est-a-dire enregistre, d'une article ou d'une page. Nous pourrons aussi ajouter des boutons a TinyMCE : https://codex.wordpress.org/TinyMCE_Custom_Buttons. Une session serait-elle interessante ?
 */

$lang = explode( '-', get_bloginfo( 'language' ) );

/**
 * Pour declarer une langue de traduction, il suffit de completer le tableau.
 */

$listTranslations = array( $lang[0], 'en' );

/**
 * Nous detectons le nom de la langue. Si aucune traduction n'est disponible dans le tableau $listTranslations ou si elle n'est pas reconnue, nous indiquons la langue par defaut, c'est-a-dire '$listTranslations[0]'.
 * Si Javascript est desactive, la detection est inoperante. C'est pourquoi, dans le theme, la balise '<html>' recoit la classe 'sr-user-lang-fr', ou 'fr' correspond a la langue definie dans les reglages de WordPress.
 */

if ( ! function_exists( 'sator_detect_lang' ) ) :
    function sator_detect_lang() {

        global $listTranslations;

        $browserLang = null;

        $userLang = null;
        
        if ( function_exists( 'locale_accept_from_http' ) ) {
            $browserLang = locale_accept_from_http( $_SERVER['HTTP_ACCEPT_LANGUAGE'] );
        }

        if ( empty( $browserLang ) ) {
            $browserLang = substr( $_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2 );
        }

        if ( ! in_array( $browserLang, $listTranslations ) ) {
            $userLang = $listTranslations[0];
        } else {
            $userLang = $browserLang;
        }

        return $userLang;
    }
endif;

/**
 * Nous passons le nom de la langue a Javascript. Si elle ne correspond pas a celle definie dans '<html>', elle sera remplacee.
 */

if ( ! function_exists( 'sator_pass_lang' ) ) :
    function sator_pass_lang() {

        global $listTranslations;

        $userLang = sator_detect_lang();
        
        $optionsSator = get_option( 'sator_settings' );
        
        if ( $optionsSator['developpmentStatus'] ) {
            wp_enqueue_script( 'sator-internationalization-js', plugins_url( 'js/sator-internationalization.js', __FILE__ ), '', '', true);
        } else {
            wp_enqueue_script( 'sator-internationalization-js', plugins_url( 'js/sator-internationalization-dist.js', __FILE__ ), '', '', true);
        }

        wp_localize_script( 'sator-internationalization-js', 'lang_objects', array( 'defaultLang' => $listTranslations[0], 'userLang' => $userLang ) );

    }
endif;
add_action( 'wp_enqueue_scripts', 'sator_pass_lang' );