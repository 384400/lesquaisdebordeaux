<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sator
 */

?>

<section class="entry__section entry__section--no-results entry__section--not-found">
    <header class="entry__header">
        <h1 class="entry__header__title"><?php esc_html_e( 'Introuvable', 'sator' );?></h1>
    </header><!-- .entry__header -->

    <div class="entry__content">
        <?php
        if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

            <p><?php
                printf(
                    wp_kses(
                        /* translators: 1: link to WP admin new post page. */
                        __( 'Êtes-vous prêt à publier votre premier article&#x202f;? <a href="%1$s">Commencez maintenant</a>.', 'sator' ),
                        array(
                            'a' => array(
                                'href' => array(),
                            ),
                        )
                    ),
                    esc_url( admin_url( 'post-new.php' ) )
                );
            ?></p>

        <?php elseif ( is_search() ) : ?>

            <p><?php esc_html_e( 'Votre recherche ne donne aucun résultat. Essayez avec d’autres mot-clefs.', 'sator' );?></p>
            <?php
                get_search_form();

        else : ?>

            <p><?php esc_html_e( 'Ce contenu n’existe pas. Une recherche pourrait vous être utile.', 'sator' );?></p>
            <?php
                get_search_form();

        endif; ?>
    </div><!-- .entry__content -->
</section><!-- .entry__section -->