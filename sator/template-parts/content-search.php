<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sator
 */

?>

<article id="post-<?php the_ID();?>" <?php post_class('entry__post'); ?>
    <header class="entry__post__header">
        <?php the_title( sprintf( '<h2 class="entry__post__title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );?>

        <?php if ( 'post' === get_post_type() ) : ?>
        <div class="entry__post__header__meta">
            <?php sator_posted_on();?>
        </div><!-- .entry__post__header__meta -->
        <?php endif;?>
    </header><!-- .entry__post__header -->

    <div class="entry__post__summary">
        <?php the_excerpt();?>
    </div><!-- .entry__post__summary -->

    <footer class="entry__post__footer">
        <?php sator_entry_footer();?>
    </footer><!-- .entry__post__footer -->
</article><!-- #post-<?php the_ID();?> -->