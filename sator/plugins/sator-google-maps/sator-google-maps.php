<?php
/**
 * Plugin Name: Sator Google Maps
 * Plugin URI: http://www.384400.xyz
 * Description: Embed Google Maps [GoogleMaps]. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

$optionsSator = get_option( 'sator_settings' );
 
/**
 * Cette fonction permet de supporter les attributs "async" et "defer" pour appeler la librarie de Google Maps.
 */

add_filter( 'script_loader_tag', 'sator_google_maps_add_async_defer_attributes', 10, 2 );

if ( ! function_exists( 'sator_google_maps_add_async_defer_attributes' ) ) :
    
    function sator_google_maps_add_async_defer_attributes( $tag, $handle ) {
        
        $scriptsToOverwrite = array( 'sator-google-maps-library' );
        
        foreach( $scriptsToOverwrite as $scriptToRewrite ) {
            
            if ( $scriptToRewrite === $handle ) {
                
                return str_replace( ' src', ' async defer src', $tag );
            }
            
        }
        
        return $tag;
    

    }

endif;

/**
 * Nous appelons la carte.
 */

add_shortcode('GoogleMaps', 'sator_create_google_maps');

if ( ! function_exists( 'sator_create_google_maps' ) ) :
    function sator_create_google_maps() {

        wp_enqueue_script( 'sator-google-maps-js' );
        wp_enqueue_script( 'sator-google-maps-library' );

        global $optionsSator;

        $latitude = $optionsSator['mappingLatitude'];
        $longitude = $optionsSator['mappingLongitude'];
        $zoom = $optionsSator['mappingZoom'];

        $displayGeo = <<<EOT
<div id="sator-map" class="entry__post__content__home__map__display mb2 mt2">
</div><!-- #sator-map .entry__post__content__home__map__display -->
<div class="lang french">
    <p class="map__failback"><a title="Nous situer sur Google Maps (S’ouvre dans une nouvelle fenêtre)" href="http://maps.google.com/?z={$zoom}&q={$latitude},{$longitude}" target="_blank">Situation sur Google Maps</a></p>
    <div class="map__gps"><p>Coordonnées <abbr title="Global Positioning System">Gps</abbr>&#x2009;: <span class="latitude">{$latitude}</span>, <span class="longitude">{$longitude}</span>.</p></div>
</div><!-- .lang .french -->
<div class="lang english">
    <p class="entry__post__content__home__map__failback"><a title="Locate us on Google Maps (Opens in new window)" href="http://maps.google.com/?z={$zoom}&q={$latitude},{$longitude}" target="_blank">Location on Google Maps</a></p>
    <div class="entry__post__content__home__map__gps"><p><abbr title="Global Positioning System">Gps</abbr> coordinates: <span class="latitude">{$latitude}</span>, <span class="longitude">{$longitude}</span>.</p></div>
</div><!-- .lang .english -->
EOT
;
        return $displayGeo;

}

endif;

/**
 * Nous affichons la carte grace a JavaScript.
 */

add_action( 'wp_enqueue_scripts', 'sator_display_google_maps' );

if ( ! function_exists( 'sator_display_google_maps' ) ) :
    function sator_display_google_maps() {

        global $optionsSator;

        $latitude = $optionsSator['mappingLatitude'];
        $longitude = $optionsSator['mappingLongitude'];
        $zoom = $optionsSator['mappingZoom'];
        $keyGoogleMaps = $optionsSator['keyGoogleMaps'];
        $tooltipText = $optionsSator['organizationName'];

        wp_register_script( 'sator-google-maps-library', 'https://maps.googleapis.com/maps/api/js?key='.$keyGoogleMaps.'&callback=initMap', '', '', '' );
        
        if ( $optionsSator['developpmentStatus'] ) {
            wp_register_script( 'sator-google-maps-js', plugins_url( 'js/sator-google-maps.js', __FILE__ ), '', '', '' );
        } else {
            wp_register_script( 'sator-google-maps-js', plugins_url( 'js/sator-google-maps-dist.js', __FILE__ ), '', '', '' );
        }

        $toLocalize = array(
            'latitude' => $latitude,
            'longitude' => $longitude,
            'zoom' => $zoom,
            'tooltipText' => $tooltipText
        );

        wp_localize_script( 'sator-google-maps-js', 'maps_objects', $toLocalize );
    }
endif;