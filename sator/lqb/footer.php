<?php
if ( is_page_template('page-home.php') || is_page ( array ('flat-a', 'flat-b', 'flat-c' ) ) ) :
?>
    <div class="site__content__distances">
        <div class="h3"><?php esc_html_e( 'Quelques distances depuis notre hôtel particulier…', 'lqb' ); ?></div>
            <div class="site__content__distances__roadmap">
            <div class="site__content__distances__roadmap__station">
                <div class="site__content__distances__roadmap__station">
                    <div class="site__content__distances__roadmap__station__item"></div>
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 25 mètres', 'lqb' ); ?></span>
                        <span><?php esc_html_e( 'Les quais', 'lqb' ); ?></span>
                        <span><?php esc_html_e( 'rive droite', 'lqb' ); ?></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 700 mètres', 'lqb' ); ?></span>
                        <span><a href="http://www.bordeaux.fr/p63920/le-jardin-botanique" target="blank" title="<?php esc_html_e( 'Aller sur la page du Jardin Botanique (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Jardin Botanique</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 700 mètres', 'lqb' ); ?></span>
                        <span>Les quais</span>
                        <span><?php esc_html_e( 'rive gauche', 'lqb' ); ?></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 1&#x2009;100 mètres', 'lqb' ); ?></span>
                        <span><a href="http://www.bordeaux.fr/p51020" target="blank" title="<?php esc_html_e( 'Aller sur la page du Parc aux Angéliques (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Parc aux Angéliques</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 1&#x2009;400 mètres', 'lqb' ); ?></span>
                        <span><a href="http://darwin.camp/" target="blank" rel="nofollow" title="<?php esc_html_e( 'Aller sur le site Darwin (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Darwin</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 1&#x2009;400 mètres', 'lqb' ); ?></span>
                        <span><a href="http://marchedescapucins.com/" target="blank" rel="nofollow" title="<?php esc_html_e( 'Aller sur le site du Marché des Capucins (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Marché des Capucins</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 1&#x2009;900 mètres', 'lqb' ); ?></span>
                        <span>Place Pey-Berland</span>
                        <span><?php esc_html_e( 'le cœur de ville', 'lqb' ); ?></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 2&#x2009;000 mètres', 'lqb' ); ?></span>
                        <span>Grand Théâtre</span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 2&#x2009;300 mètres', 'lqb' ); ?></span>
                        <span>Place Gambetta</span>
                    </div><!-- .station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 2&#x2009;700 mètres', 'lqb' ); ?></span>
                        <span><a href="http://www.bordeaux.fr/l858" target="blank" title="<?php esc_html_e( 'Aller sur la page du Jardin Public (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Jardin Public</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 3&#x2009;900 mètres', 'lqb' ); ?></span>
                        <span><a href="http://www.laciteduvin.com/" target="blank" rel="nofollow" title="<?php esc_html_e( 'Aller sur le site de La Cité du Vin (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Cité du Vin</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 6&#x2009;500 mètres', 'lqb' ); ?></span>
                        <span><a href="http://www.bordeaux.fr/o5189/plage-du-lac" target="blank" title="<?php esc_html_e( 'Aller sur la page de la Plage du Lac (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Plage du Lac</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 6&#x2009;800 mètres', 'lqb' ); ?></span>
                        <span><a href="http://www.bordeaux-events.com/" target="blank" rel="nofollow" title="<?php esc_html_e( 'Aller sur la page du Palais des Congrès (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Palais des Congrès</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 7&#x2009;900 mètres', 'lqb' ); ?></span>
                        <span><a href="http://www.matmut-atlantique.com/" rel="nofollow" target="blank" title="<?php esc_html_e( 'Aller sur le site du Stade Matmut Atlantique (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Stade Matmut Atlantique</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                    <div class="site__content__distances__roadmap__station__item">
                        <div class="site__content__distances__roadmap__station__item__icon"></div>
                        <span><?php esc_html_e( 'À 8&#x2009;300 mètres', 'lqb' ); ?></span>
                        <span><a href="http://www.bordeaux-events.com/Notre-offre/Nos-lieux/Parc-des-Expositions" target="blank" rel="nofollow" title="<?php esc_html_e( 'Aller sur la page du Parc des Expositions du Stade Matmut Atlantique (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Parc des Expositions</a></span>
                    </div><!-- .site__content__distances__roadmap__station__item -->
                </div><!-- .site__content__distances__roadmap__station -->
            </div><!-- .site__content__distances__roadmap__station -->
        </div><!-- .site__content_distances__roadmap -->
    </div><!-- .site__content__distances-->
<?php
endif;
?>
<div id="legal"> 
</div>
    </div><!-- #content site__content -->

    <footer id="colophon" class="site__footer">
        <div class="footer__share legacy-dn">
            <a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink());?>" rel="nofollow" target="_blank" title="<?php esc_html_e( 'Partager ce contenu sur Facebook (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">
                <svg>
                    <use xlink:href="#facebook-box"></use>
                </svg>
            </a>
            <a href="https://twitter.com/share?text=<?php echo urlencode( get_the_title() );?>" rel="nofollow" target="_blank" title="<?php esc_html_e( 'Partager ce contenu sur Twitter (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">
                <svg>
                    <use xlink:href="#twitter-box"></use>
                </svg>
            </a>
            <a href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink());?>" target="_blank" rel="nofollow" title="<?php esc_html_e( 'Partager ce contenu sur Google + (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">
                <svg>
                    <use xlink:href="#google-plus-box"></use>
                </svg>
            </a>
        </div><!-- .footer__share -->
        <div class="footer__info">
            <p><span class="footer__info__copyright"><span><?php printf( ( '%1$s %2$s</span>' ), date( 'Y' ), get_bloginfo( 'name' ) ); ?></span></span>
            <span class="footer__info__sep"> | </span>
            <?php
                printf( ( 'Conçu par %1$s.' ), '<a href="http://www.384400.xyz/">384400</a>' );
            ?></p>
            <div class="footer__info__totop">
                <div class="trendy-dn">
                    <p class="no-support-js"><a href="#page" title="<?php esc_html_e( 'Retourner en haut de page', 'lqb' ); ?>"><?php esc_html_e( 'Retour en haut de page', 'lqb' ); ?></a></p>
                    <p class="support-js legacy-dn"><a href="javascript:;" onclick="document.documentElement.scrollTop=0; return false;" title="<?php esc_html_e( 'Retourner en haut de page', 'lqb' ); ?>"><?php esc_html_e( 'Retour en haut de page', 'lqb' ); ?></a></p>
                </div>
                <div class="legacy-dn">
                    <a href="javascript:;" onclick="document.documentElement.scrollTop=0; return false;" title="<?php esc_html_e( 'Retourner en haut de page', 'lqb' ); ?>">
                        <svg>
                            <use xlink:href="#restore-page-icon"></use>
                        </svg>
                    </a>
                </div>
            </div><!-- .footer__info__totop -->
        </div><!-- .footer__info -->
    </footer><!-- #colophon .site__footer -->
    <?php if( is_page_template( 'page-home.php') ) :
        $pictures =
        [
            [
                'src' => 'commons-areas-1',
                'title' => esc_html__( 'Couloir', 'lqb' )
            ],
            [
                'src' => 'commons-areas-2',
                'title' => esc_html__( 'Escalier', 'lqb' )
            ],
            [
                'src' => 'commons-areas-3',
                'title' => esc_html__( 'Escalier', 'lqb' )
            ],
            [
                'src' => 'decoration-1',
                'title' => ''
            ],
            [
                'src' => 'decoration-1',
                'title' => ''
            ],
            [
                'src' => 'place-1',
                'title' => 'Place de Stalingrad'
            ],
            [
                'src' => 'place-2',
                'title' => 'Place de Stalingrad'
            ],
            [
                'src' => 'place-3',
                'title' => 'Place de Stalingrad'
            ],
        ];
        shuffle( $pictures );
        $src = $pictures[0]['src'];
        $title = $pictures[0]['title'];
        ?>
        <div class="site__appendix">
            <picture>
                <source media="(min-width: 48em)"
                        srcset="
                                <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src;?>-640.jpg 640w,
                                <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src;?>-1280.jpg 1280w,
                                <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src;?>-1920.jpg 1920w,
                                <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src;?>-2560.jpg 2560w"
                        sizes="100vw" />
                <source srcset="
                            <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src;?>-640.jpg 1x,
                            <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src;?>-1280.jpg 2x,
                            <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src;?>-1920.jpg 3x" />
                <img src="<?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src;?>-640.jpg" alt="<?php echo $title ?>" title="<?php echo $title ?>" />
            </picture>
        </div><!-- .site__appendix -->
    <?php 
    endif;
    ?>
</div><!-- #page .site -->

<?php wp_footer();?>

</body>
</html>