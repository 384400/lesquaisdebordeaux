<?php
/**
 * Plugin Name: Sator Preloader
 * Plugin URI: http://www.384400.xyz
 * Description: A simple and experimental way to preloading content with Javascript. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

/**
 * https://developer.mozilla.org/fr/docs/Web/HTML/Pr%C3%A9charger_du_contenu
 * https://developers.google.com/web/updates/2016/03/link-rel-preload
 * https://developer.mozilla.org/en-US/docs/Web/Events/readystatechange
 * https://developer.mozilla.org/fr/docs/Web/Events/DOMContentLoaded
 * La methode d'appel du script avec 'echo' n'est pas completement satisfaisante !
 */


if ( ! function_exists( 'sator_preload_scripts' ) ) :
    function sator_preload_scripts() {

        $optionsSator = get_option( 'sator_settings' );

        if ( $optionsSator['developpmentStatus'] ) {
            $output = '<link rel="preload" href="'.plugins_url( 'js/sator-preloader.js', __FILE__ ).'" as="script">'."\n";
        } else {
            $output = '<link rel="preload" href="'.plugins_url( 'js/sator-preloader-dist.js', __FILE__ ).'" as="script">'."\n";
        }

        echo $output;
    }
endif;
add_action( 'wp_head', 'sator_preload_scripts', 2 );