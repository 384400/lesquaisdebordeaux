<?php
/**
 * Plugin Name: Sator Fonts
 * Plugin URI: http://www.384400.xyz
 * Description: Manage custom fonts. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

/**
 * La methode d'appel du script avec 'echo' n'est pas completement satisfaisante !
 */

if ( ! function_exists( 'sator_preconnect_fonts' ) ) :
    function sator_preconnect_fonts() {

    $output = '<link rel="preconnect" media="screen" href="https://fonts.gstatic.com/" crossorigin>'."\n";
    echo $output;

}
endif;
add_action( 'wp_head', 'sator_preconnect_fonts', 2 );

if ( ! function_exists( 'sator_connect_fonts' ) ) :
    function sator_connect_fonts() {

        wp_enqueue_style( 'titillium-font', 'https://fonts.googleapis.com/css?family=Titillium+Web:400,700', false );

        wp_enqueue_style( 'amaranth-font', 'https://fonts.googleapis.com/css?family=Amaranth:700?text=Les%20quais%20de%20Bordeaux', false );

}
endif;
add_action( 'wp_enqueue_scripts', 'sator_connect_fonts' );