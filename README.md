# LISEZ-MOI #

Ce dépôt contient les fichiers de développement du site [Les quais de Bordeaux](https://lqb.384400.space/). Il s'agit d'une adresse de démonstration, dont le référencement est désactivé.

Le site fonctionne avec [WordPress](https://wordpress.org/).

Le dépôt contient le thème parent, **Sator**, le thème enfant *Lqb*, et les extensions associées. Elles furent développées par Vincent Top-Roulet consécutivement à sa formation de concepteur Web suivie au [Centre d'Études Supérieures Industrielles](http://www.cesi.fr/) (Cesi) de Bordeaux en 2017.

Le cas échéant, les fichiers en production portent le suffixe *dist*.



S A T O R
=========
A R E P O
=========
T E N E T
=========
O P E R A
=========
R O T A S
=========