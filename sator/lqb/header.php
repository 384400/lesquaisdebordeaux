<?php
$lang = explode( '-', get_bloginfo( 'language' ) );
?>
<!doctype html>
<!-- "Never lose a holy curiosity." (Albert Einstein) -->
<html <?php language_attributes(); ?> class="sr-nojs sr-js-legacy sr-user-lang-<?php echo $lang[0];?>">
<head>
    <meta charset="<?php bloginfo( 'charset' );?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head();?>
</head>

<body <?php body_class();?>>
<?php get_template_part( 'library/svg/sprites.svg' );?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Aller au contenu', 'sator' );?></a>

    <header id="masthead" class="site__header">
        
        <div id="outdated" class="header__outdated trendy-dn">
            <p class="no-support-js"><strong><?php esc_html_e( 'Comme votre navigateur ne prend pas en charge Javascript, une version allégée du site s’affiche.', 'sator' ); ?></strong></p>
            <div class="support-js legacy-dn">
                <p><strong><?php esc_html_e( 'Comme votre navigateur ne prend pas en charge certaines fonctionnalités, une version simplifiée du site s’affiche.', 'sator' ); ?></strong></p>
                <p><a href="javascript:;" onclick="closeAlertLegacy(); return false;" title="<?php esc_html_e( 'Fermer le message d’avertissement', 'sator' ); ?>">&#x2713;<?php esc_html_e( '&#x00A0;Entendu.', 'sator' ); ?></a></p>
            </div>
        </div>

        <nav id="navigation">
            <div class="site__navigation legacy-dn">
                <details class="site__navigation__menu">
                    <summary></summary>
                        <h2><?php esc_html_e( 'Navigation', 'lqb' ); ?></h2>
                        <?php 
                            /* "container' => 'false'" ne fonctionne pas. */ 
                            wp_nav_menu( array( 
                                'container'         => ' ', 
                                'menu_id'           => 'primary-trendy-menu',
                                'menu_class'        => 'trendy-menu',
                                'sort_column'       => 'menu_order',
                                'theme_location'    => 'menu-1'
                            ) );
                        ?>
                </details>
            </div>
            <div class="nojs-tac legacy-tac trendy-dn">
                <h2><?php esc_html_e( 'Navigation', 'lqb' ); ?></h2>
                <?php 
                    wp_nav_menu( array( 
                        'container'         => '', 
                        'menu_id'           => 'primary-legacy-menu',
                        'menu_class'        => 'legacy-menu',
                        'sort_column'       => 'menu_order',
                        'theme_location'    => 'menu-1'
                    ) ); 
                ?>
            </div>

        </nav><!-- #navigation .site__navigation -->

        <div class="header__branding">
            <h1><a href="<?php echo esc_url( home_url( '/' ) );?>" rel="home"><?php bloginfo( 'name' );?></a></h1>
            <div>
                <picture>
                    <source srcset="<?php echo get_stylesheet_directory_uri();?>/library/img/orlane-marisol-480.jpg 1x, <?php echo get_stylesheet_directory_uri();?>/library/img/orlane-marisol-640.jpg 2x, <?php echo get_stylesheet_directory_uri();?>/library/img/orlane-marisol-1280.jpg 3x" media="(min-width: 160em)">
                    <source srcset="<?php echo get_stylesheet_directory_uri();?>/library/img/orlane-marisol-240.jpg 1x, <?php echo get_stylesheet_directory_uri();?>/library/img/orlane-marisol-480.jpg 2x, <?php echo get_stylesheet_directory_uri();?>/library/img/orlane-marisol-640.jpg 3x"><img src="<?php echo get_stylesheet_directory_uri();?>/library/img/orlane-marisol-240.jpg" alt="Orlane et Marisol" title="Orlane et Marisol">
                </picture>
            </div>
        </div><!-- .header__branding -->

       <?php 
        if ( is_page_template( 'home.php' ) || is_page_template( 'flat.php' ) ) :
        ?>
            <div class="header__ads <?php echo( is_page_template( 'home.php' ) ? 'header__ads--home' : 'header__ads--page');?>">
                <div><a href="https://www.airbnb.fr/" target="_blank" title="<?php esc_html_e( 'Aller sur le site Airbnb (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>"><img src="<?php echo get_stylesheet_directory_uri();?>/library/img/airbnb.png" alt="" /></a></div>
                <?php 
                if ( is_page_template( 'home.php') ) :
                ?>
                    <div>
                        <p>Accédez à nos trois annonces sur <a href="https://www.airbnb.fr/" rel="nofollow" target="_blank" title="Aller sur le site Airbnb (S’ouvre dans une nouvelle fenêtre)">Airbnb</a> pour connaître les disponibilités et réserver…</p>
                        <ul><li><a href="https://www.airbnb.fr/rooms/20778908" target="_blank" title="Voir sur Airbnb l’annonce de notre premier appartement (S’ouvre dans une nouvelle fenêtre)">1</a></li><li><a href="https://www.airbnb.fr/rooms/20766817" target="_blank" title="Voir sur Airbnb l’annonce de notre deuxième appartement (S’ouvre dans une nouvelle fenêtre)">2</a></li><li><a href="https://www.airbnb.fr/rooms/16990947" target="_blank" title="Voir sur Airbnb l’annonce de notre troisième appartement (S’ouvre dans une nouvelle fenêtre)">3</a></li></ul>
                    </div>
                <?php 
                else :
                ?>
                    <div>
                        <?php
                        if ( is_page ( 'flat-a' ) ) :
                        ?>
                            <p>Consultez notre <a href="https://www.airbnb.fr/rooms/20778908" target="_blank" title="Voir notre annonce sur Airbnb (S’ouvre dans une nouvelle fenêtre)">annonce</a> pour connaître la disponibilité et réserver.</p>
                            <div class="h4">Autres appartements…</div>
                            <ul><li><a href="<?php echo get_template_directory_uri();?>/flat-b" title="Voir les images de l’autre appartement">A</a></li><li><a href="<?php echo get_template_directory_uri();?>/flat-c" title="Voir les images de l'autre appartement">B</a></li></ul>
                        <?php
                        endif;
                        if ( is_page ( 'flat-b' ) ) :
                        ?>
                            <p>Consultez notre <a href="https://www.airbnb.fr/rooms/20766817" target="_blank" title="Voir notre annonce sur Airbnb (S’ouvre dans une nouvelle fenêtre)">annonce</a> pour connaître la disponibilité et réserver.</p>
                            <div class="h4">Autres appartements…</div>
                            <ul><li><a href="<?php echo get_template_directory_uri();?>/flat-a" title="Voir les images de l’autre appartement">A</a></li><li><a href="<?php echo get_template_directory_uri();?>/flat-c" title="Voir les images de l'autre appartement">B</a></li></ul>
                        <?php
                        endif;
                        if ( is_page ( 'flat-c' ) ) :
                        ?>
                            <p>Consultez notre <a href="https://www.airbnb.fr/rooms/16990947" target="_blank" title="Voir notre annonce sur Airbnb (S’ouvre dans une nouvelle fenêtre)">annonce</a> pour connaître la disponibilité et réserver.</p>
                            <div class="h4">Autres appartements…</div>
                            <ul><li><a href="<?php echo get_template_directory_uri();?>/flat-a" title="Voir les images de l’autre appartement">A</a></li><li><a href="<?php echo get_template_directory_uri();?>/flat-b" title="Voir les images de l'autre appartement">B</a></li></ul>
                        <?php
                        endif;
                        ?>
                    </div>
                <?php 
                endif;
                ?>
            </div><!-- .ads -->
        <?php 
        endif;
        ?>
    
    </header><!-- #masthead .site__header -->

    <div id="content" class="site__content">