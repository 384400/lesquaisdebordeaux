<?php
/**
 * Plugin Name: Sator Geo Tags
 * Plugin URI: http://www.384400.xyz
 * Description: Add geo tags to post and page. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

/**
 * Nous creons les champs personnalises.
 */

if ( ! function_exists( 'sator_create_geo_tags' ) ) :
    function sator_create_geo_tags( $post_id ) {

        add_post_meta( $post_id, 'geo_region', '', true );

        add_post_meta( $post_id, 'geo_placename', '', true );

        add_post_meta( $post_id, 'geo_latitude', '', true );

        add_post_meta( $post_id, 'geo_longitude', '', true );
    }
endif;
add_action( 'wp_insert_post', 'sator_create_geo_tags', 1 );

/**
 * Nous affichons les champs personnalises.
 */

if ( ! function_exists( 'sator_display_geo_tags' ) ) :
    function sator_display_geo_tags() {

        global $post;
        
        $contentGeoTags = null;
    
        /* http://www.geo-tag.de/generator/en.html
            <meta name="geo.region" content="FR" />
            <meta name="geo.placename" content="Bordeaux" />
            <meta name="geo.position" content="44.8398330;-0.5593690" />
            <meta name="ICBM" content="44.8398330, -0.5593690" /> 
        */
        $geoRegion = get_post_meta( $post->ID, 'geo_region', true );
        /* e.g. "FR-33", cf http://geotags.com/iso3166/iso.FR.html */
        $geoPlacename = get_post_meta( $post->ID, 'geo_placename', true );
        /*  e.g. "Bordeaux", cf. http://www.getty.edu/research/tools/vocabularies/tgn/ */
        $geoLatitude = get_post_meta( $post->ID, 'geo_latitude', true );
        $geoLongitude = get_post_meta( $post->ID, 'geo_longitude', true );
    
        if ( $geoRegion && $geoPlacename && $geoLatitude && $geoLongitude ) {
            $contentGeoTags .= '<meta name="geo.region" content="'.$geoRegion.'" />';
            $contentGeoTags .= '<meta name="geo.placename" content="'.$geoPlacename.'" />';
            $contentGeoTags .= '<meta name="geo.position" content="'.$geoLatitude.';'.$geoLongitude.'" />';
            $contentGeoTags .= '<meta name="ICBM" content="'.$geoLatitude.', '.$geoLongitude.'" />';
            print $contentGeoTags;
        }
}
endif;
add_action( 'wp_head', 'sator_display_geo_tags' );