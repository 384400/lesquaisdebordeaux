<?php
/**
 * Plugin Name: Sator Twitter
 * Description: Embed Twitter Timeline [TwitterTimeline account="" color="" title="" theme=""]. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

$optionsSator = get_option( 'sator_settings' );
 
/**
 * Cette fonction permet de supporter les attributs "async" et "charset" pour appeler la librarie de Twitter.
 */

if ( ! function_exists( 'sator_twitter_add_async_charset_attributes' ) ) :
    function sator_twitter_add_async_charset_attributes( $tag, $handle ) {

        $scriptsToOverwrite = array( 'sator-twitter-library' );

        foreach( $scriptsToOverwrite as $scriptToRewrite ) {    
            if ( $scriptToRewrite === $handle ) {   
                return str_replace( ' src', ' charset=\'utf-8\' async src', $tag );
            }   
        }
        
        return $tag;
}
endif;
add_filter( 'script_loader_tag', 'sator_twitter_add_async_charset_attributes', 10, 2 );

/**
 * Nous appelons la Timeline.
 */

if ( ! function_exists( 'sator_create_timeline' ) ) :
    function sator_create_timeline( $atts ) {

        /* https://dev.twitter.com/web/embedded-timelines/parameters */

        wp_enqueue_script( 'sator-twitter-library' );

        if ( ! empty($atts['theme'] ) ) {
        $displayTimeline = <<<EOT
<div>
    <h3>Notre actualité</h3>
    <div class="timeline">
        <a class="twitter-timeline" data-aria-polite="assertive" data-border-color="{$atts['color']}" data-chrome="noscrollbar" data-dnt="false" data-link-color="{$atts['color']}" data-link-color="{$atts['color']}" data-theme="{$atts['theme']}" href="https://twitter.com/{$atts['account']}?ref_src=twsrc%5Etfw">{$atts['title']}</a>
    </div>
</div>
EOT
;
        } else {
        $displayTimeline = <<<EOT
<div>
    <h3>Notre actualité</h3>
    <div class="timeline">
        <a class="twitter-timeline" data-aria-polite="assertive" data-border-color="{$atts['color']}" data-chrome="noscrollbar transparent" data-dnt="false" data-link-color="{$atts['color']}" href="https://twitter.com/{$atts['account']}?ref_src=twsrc%5Etfw">{$atts['title']}</a>
    </div>
</div>
EOT
;
        }
    return $displayTimeline;

}
endif;
add_shortcode( 'TwitterTimeline', 'sator_create_timeline' );

/**
 * Nous appelons la librairie de Twitter.
 */

add_action( 'wp_enqueue_scripts', 'sator_write_twitter_timeline' );

if ( ! function_exists( 'sator_write_twitter_timeline' ) ) :
    function sator_write_twitter_timeline() {

        wp_register_script( 'sator-twitter-library', 'https://platform.twitter.com/widgets.js', '', '', true );

    }
endif;