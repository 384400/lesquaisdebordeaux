<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sator
 */

?>
<!doctype html>
<!-- "Never lose a holy curiosity." (Albert Einstein) -->
<html <?php language_attributes(); ?> class="sr-nojs sr-js-legacy">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Aller au contenu', 'sator' ); ?></a>

    <header id="masthead" class="site__header">
        
        <div id="outdated" class="header__outdated trendy-dn">
            <p class="no-support-js"><strong><?php esc_html_e( 'Comme votre navigateur ne prend pas en charge Javascript, une version allégée du site s’affiche.', 'sator' ); ?></strong></p>
            <div class="support-js legacy-dn">
                <p><strong><?php esc_html_e( 'Comme votre navigateur ne prend pas en charge certaines fonctionnalités, une version simplifiée du site s’affiche.', 'sator' ); ?></strong></p>
                <p><a href="javascript:;" onclick="closeAlertLegacy(); return false;" title="<?php esc_html_e( 'Fermer le message d’avertissement', 'sator' ); ?>">&#x2713;<?php esc_html_e( '&#x00A0;Entendu.', 'sator' ); ?></a></p>
            </div>
        </div>

        <nav id="navigation" class="site__navigation">
            <details class="menu">
                <summary></summary>
                    <h2>Navigation</h2>
                    <?php
                        /* "container' => 'false'" ne fonctionne pas. */ 
                        wp_nav_menu( array(
                            'container'          => ' ', 
                            'menu_id'            => 'primary-menu',
                            'sort_column'        => 'menu_order',
                            'theme_location'     => 'menu-1'
                        ) );
                    ?>
            </details>
        </nav><!-- #navigation .site__navigation -->

        <div class="header__branding">
            <?php
            if ( is_front_page() && is_home() ) : ?>
                <h1 class="header__title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <?php else : ?>
                <p class="header__title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
            <?php
            endif;

            $description = get_bloginfo( 'description', 'display' );
            if ( $description ) : ?>
                <p class="header__description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
            <?php
            endif; ?>
        </div><!-- .header__branding -->

    </header><!-- #masthead .site__header -->

<div id="content" class="site__content">