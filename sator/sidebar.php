<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sator
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
    return;
}
?>

<aside id="secondary" class="site__content__secondary">
    <?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary .site__content__secondary -->