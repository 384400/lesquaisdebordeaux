var ajax_objects;
var form = document.querySelector('#contact');
var button = document.querySelector('#send');
button.addEventListener('click', function(event) {
    if (form.checkValidity()) {
        if (document.body.parentNode.classList.contains('sr-js-trendy')) {
            document.querySelector('#sator-response').innerHTML = '<svg class="entry__post__content__form__response__spinner" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="entry__post__content__form__response__spinner--path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg>';
        } else {
            document.querySelector('#sator-response').innerHTML = '<div class="entry__post__content__form__response__loader">' + ajax_objects.loadingMsg + '</div>';
        }

        var formElement = document.querySelector('#contact');
        var formData = new FormData(formElement);
        
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status === 200) {
        
                    var removeCheck = document.querySelectorAll('.entry__post__content__form__check');
                    
                    for (var i = 0;i < removeCheck.length;i++) {
                        removeCheck[i].innerHTML = '';
                    }
                    
                    var json = JSON.parse(xhr.response);

                    var hasError = 0;
                    if (json.data.errors) {
                        var errorKeys = Object.keys(json.data.errors);
                        var errorValues = Object.values(json.data.errors);
                        for (var prop in errorKeys) {
                            document.querySelector('#' + errorKeys[prop] + '-check').innerHTML = '<p>' + errorValues[prop] + '</p>';
                            hasError++;
                        }
                    }
                    if (hasError) {
                        if (document.body.parentNode.classList.contains('sr-js-trendy')) {
                            document.querySelector('#sator-response').innerHTML = '<div class="entry__post__content__form__response__process"><div class="entry__post__content__form__response__process--error"></div><div><p>' + json.data.feedback + '</p></div></div>';
                        } else {
                            document.querySelector('#sator-response').innerHTML = '<div class="entry__post__content__form__response__process entry__post__content__form__response__process--error"><div><p>' + json.data.feedback + '</p></div></div>';
                        }
                    } else {
                        if (document.body.parentNode.classList.contains('sr-js-trendy')) {
                            document.querySelector('#sator-response').innerHTML = '<div class="entry__post__content__form__response__process"><div class="entry__post__content__form__response__process--success"></div><div><p>' + json.data.feedback + '</p></div></div>';
                        } else {
                            document.querySelector('#sator-response').innerHTML = '<div class="entry__post__content__form__response__process entry__post__content__form__response__process--error"><div></p>' + json.data.feedback + '</p></div></div>';
                        }
                        document.querySelector('#contact').classList.add('dn');
                    }
                } else {
                    console.log(xhr.statusText);
                }
            }
        };
        xhr.open('post', ajax_objects.ajaxUrl, true);
        xhr.send(formData);
        event.preventDefault(event);
    }
});