<aside id="secondary" class="site__content__secondary <?php echo( is_page_template( 'page-home.php' ) ? 'site__content__secondary--home' : 'site__content__secondary--page'); ?>">
    <?php if( is_page_template( 'home.php' ) ) : ?>
        <?php 
            echo do_shortcode('[TwitterTimeline account="quaisdebordeaux" color="#009688" theme="dark" title="' .esc_html__( 'Nos tweets', 'lqb' ).'"]');
        ?>
    <?php endif; ?>
    <?php if ( ! is_page_template( 'flat.php' ) ) : ?>
        <div>
            <h3>Liens utiles</h3>
            <ul>
                <li>
                    <ul>
                        <li><a href="https://www.bordeauxmetropolearena.com/" target="blank" title="<?php esc_html_e( 'Aller sur le site de Bordeaux Métropole Arena (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Bordeaux Métropole Arena</a></li>
                        <li><a href="http://www.cap-sciences.net/" target="blank" title="<?php esc_html_e( 'Aller sur le site de Cap Sciences (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Cap Sciences</a></li>
                        <li><a href="http://www.capc-bordeaux.fr/" target="blank" title="<?php esc_html_e( 'Aller sur le site du Centre d’Arts Plastiques Contemporains (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Capc musée d’art contemporain</a></li>
                        <li><a href="http://darwin.camp/" target="blank" title="<?php esc_html_e( 'Aller sur le site Darwin (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Darwin</a></li>
                        <li><a href="http://www.laciteduvin.com/" target="blank" title="<?php esc_html_e( 'Aller sur le site de La Cité du Vin (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">La Cité du Vin</a></li>
                        <li><a href="http://marchedescapucins.com/" target="blank" title="<?php esc_html_e( 'Aller sur du Marché des Capucins (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Marché des Capucins</a></li>
                        <li><a href="http://www.matmut-atlantique.com/" target="blank" title="<?php esc_html_e( 'Aller sur le site du Matmut Atlantique (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?> ">Matmut Atlantique</a></li>
                        <li><a href="http://www.musee-aquitaine-bordeaux.fr/" target="blank" title="<?php esc_html_e( 'Aller sur le site du Musée d’Aquitaine (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Musée d’Aquitaine</a></li>
                        <li><a href="http://www.madd-bordeaux.fr/" target="blank" title="<?php esc_html_e( 'Aller sur le site du Musée des Arts décoratifs et du Design (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Musée des Arts décoratifs et du Design</a></li>
                        <li><a href="http://www.musba-bordeaux.fr/" target="blank" title="<?php esc_html_e( 'Aller sur le site du Musée des Beaux Arts (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Musée des Beaux Arts</a></li>
                        <li><a href="http://www.musee-douanes.fr/" target="blank" title="<?php esc_html_e( 'Aller sur le site du Musée National des Douanes (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Musée National des Douanes</a></li>
                        <li><a href="https://www.opera-bordeaux.com/" target="blank" title="<?php esc_html_e( 'Aller sur le site de l’Opéra National de Bordeaux (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Opéra National de Bordeaux</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <li><a href="http://www.bordeaux-tourisme.com/" target="blank" title="<?php esc_html_e( 'Aller sur le site de Bordeaux Tourisme et Congrès (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Bordeaux Tourisme &#x26; Congrès</a></li>
                        <li><a href="http://www.bordeaux-events.com/" target="blank" title="<?php esc_html_e( 'Aller sur le site Bordeaux Events (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Bordeaux Events</a></li><li><a href="http://www.bordeaux-expo.com/" target="blank" title="<?php esc_html_e( 'Aller sur le site de Congrès &#x26; Expositions de Bordeaux (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Congrès &#x26; Expositions de Bordeaux</a></li>
                        <li><a href="http://www.bordeaux.fr/" target="blank" title="<?php esc_html_e( 'Aller sur le site de la Ville de Bordeaux (S’ouvre dans une nouvelle fenêtre)', 'lqb' ); ?>">Ville de Bordeaux</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    <?php 
    endif;
    if( is_page_template( 'flat.php' ) ) :
    ?>
        <h3>Accès</h3>
        <p>Une bagagerie est gracieusement mise à disposition de nos hôtes sans limitation de durée. Un parking privé, à l’arrière de l’immeuble, est disponible, sur réservation, au tarif négocié de 12 euros par nuit.</p>
        <h3>Échanges</h3>
        <p>Nous sommes joignables à tout moment ; nous aiderons et renseignerons nos visiteurs avec plaisir.</p>
        <h3>Équipements</h3>
        <p>Nos appartements disposent tous d’une connexion Wifi, d’un téléviseur, d’un chauffage individuel, d’une cuisine équipée, d’une salle de bains avec produits et accessoires essentiels, d’un fer à repasser et de cintres. Une laverie est disponible dans les parties communes.</p>
        <h3>Règlement intérieur</h3>
        <p>L’entrée dans les lieux intervient à partir de 14 h 00 pour un départ à 12 h 00.</p>
        <p>Le montant de la caution s’élève à 360 euros.</p>
        <p>Les appartements sont non-fumeurs, ainsi que les parties communes. Les animaux ne sont pas acceptés. La discrétion est de rigueur après 22 h 00. Fêtes ou soirées sont proscrites. Enfin, nos logis ne conviennent pas aux enfants âgés de moins de deux ans.</p>
    <?php 
    endif;
    ?>
    <?php dynamic_sidebar( 'sidebar-1' );?>
</aside><!-- #secondary .site__content__secondary -->