var maps_objects;

/*
Nous convertissons les trois chaines de caracteres passees par "wp_localize_script" en nombre.
*/

var latitude = +maps_objects.latitude;
var longitude = +maps_objects.longitude;
var zoom = +maps_objects.zoom;
var tooltipText = maps_objects.tooltipText;

var map;

function initMap() {
    var coordinates = {
        lat: latitude,
        lng: longitude
    };
    var mapOptions = {
        center: coordinates,
        zoom: zoom
    };
    map = new google.maps.Map(document.querySelector('#sator-map'), mapOptions);
    var markerOptions = {
        position: coordinates,
        map: map,
        title: tooltipText
    };
    var marker = new google.maps.Marker(markerOptions);
    /* 'Load' est-il necessaire ? */
    google.maps.event.addDomListener(window, 'load', initMap);
    google.maps.event.addDomListener(window, 'resize', function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, 'resize');
        map.setCenter(center);
    });
}