<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sator
 */

?>

<article id="post-<?php the_ID();?>" <?php post_class('entry__post');?>>
    <header class="entry__post__header">
        <?php the_title( '<h1 class="entry__post__title">', '</h1>' );?>
    </header><!-- .entry__post__header -->

    <div class="entry__post__content">
        <?php
            the_content();

            wp_link_pages( array(
                'before' => '<div class="entry__post__content__page-links">' . esc_html__( 'Pages&#x202f;:', 'sator' ),
                'after'  => '</div>',
            ) );
        ?>
    </div><!-- .entry__post__content -->

    <?php if ( get_edit_post_link() ) : ?>
        <footer class="entry__post__footer">
            <?php
                edit_post_link(
                    sprintf(
                        wp_kses(
                            /* translators: %s: Name of current post. Only visible to screen readers */
                            __( 'Modifier <span class="screen-reader-text">%s</span>', 'sator' ),
                            array(
                                'span' => array(
                                    'class' => array(),
                                ),
                            )
                        ),
                        get_the_title()
                    ),
                    '<span class="entry__post__footer__edit-link">',
                    '</span>'
                );
            ?>
        </footer><!-- .entry__post__footer -->
    <?php endif;?>
</article><!-- #post-<?php the_ID();?> -->