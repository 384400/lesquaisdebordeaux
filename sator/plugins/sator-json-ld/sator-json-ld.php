<?php
/**
 * Plugin Name: Sator Json Ld
 * Plugin URI: http://www.384400.xyz
 * Description: Embed JavaScript Object Notation for Linked Data. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

/**
 * A utiliser avec le champ personnalise "json_ld", en omettant les balises d'ouverture et de fermeture de Javascript.
 * Ressources
 * http://microdatagenerator.org/localbusiness-microdata-generator%20/
 * https://webcode.tools/microdata-generator
 */

/**
 * Nous creons le champ personnalise.
 */

if ( ! function_exists( 'sator_create_json_ld' ) ) :
    function sator_create_json_ld( $post_id ) {

        add_post_meta( $post_id, 'json_ld', '', true );

    }
endif;
add_action( 'wp_insert_post', 'sator_create_json_ld', 1 );

/**
 * Nous affichons le contenu du champ personnalise dans l'en-tete de la page.
 */

if ( ! function_exists( 'sator_display_json_ld' ) ) :
    function sator_display_json_ld() {

        global $post;

        $jsonLd = null;
 
        $jsonLd = get_post_meta( $post->ID, 'json_ld', true );

        if ( ! empty($jsonLd ) ) {
            /*$displayJsonLd = <<<EOT
<script type="application/ld+json">
<![CDATA[
{$jsonLd}
// ]]>
</script>
EOT
;*/
            $displayJsonLd = <<<EOT
<script type="application/ld+json">
{$jsonLd}
</script>
EOT
;
        }
        print $displayJsonLd;
    }
endif;
add_action( 'wp_head', 'sator_display_json_ld' );