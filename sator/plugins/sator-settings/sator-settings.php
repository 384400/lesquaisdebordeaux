<?php
/**
 * Plugin Name: Sator Settings
 * Plugin URI: http://www.384400.xyz
 * Description: Settings and dashboard. Sator theme and plugins are required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

/**
 * Inspire par https://gist.github.com/annalinneajohansson/5290405
 * Voir aussi https://codex.wordpress.org/Function_Reference/selected et https://codex.wordpress.org/Function_Reference/checked
 */

/**
 * Nous initialisons les variables pour eviter l'affichage d'erreurs quand l'extension n'a pas ete encore activee.
 */

$defineSettings = [
    'developpmentStatus' => '',
    'administratorIdentity' => '',
    'administratorEmail' => '',
    'keyCaptchaPublic' => '',
    'keyCaptchaPrivate' => '',
    'keyGoogleMaps' => '',
    'idGoogleAnalytics' => '',
    'organizationName' => '',
    'editorIdentity' => '',
    'editorEmail' => '',
    'editorAddress' => '',
    'editorZip' => '',
    'editorCity' => '',
    'editorCountry' => '',
    'hostName' => '',
    'hostAddress' => '',
    'hostZip' => '',
    'hostCity' => '',
    'hostCountry' => '',
    'mappingRegion' => '',
    'mappingPlacename' => '',
    'mappingLatitude' => '',
    'mappingLongitude' => '',
    'mappingZoom' => '',
    'creditsText' => ''
];

/**
 * Si la version de Php est ancienne, nous utilisons...
 * $defineSettings = array(
    'developpmentStatus' => '',
    'administratorIdentity' => ''
    );
 */

/**
 * Il y a un bogue quand la valeur d'une case a cocher est vide avec l'appel $value = esc_attr( $settings[$field] );".
 * https://stackoverflow.com/questions/22579296/notice-undefined-index-in-wordpress-plugin suggere "$value = isset($settings[$field]) ? esc_attr($settings[field]) : '';
 * https://wordpress.stackexchange.com/questions/239686/settings-api-undefined-index-when-unchecking-checkbox suggère $value = esc_attr( $settings['field'] = ! empty( $settings['field'] ) ? 1 : 0 );". C'est la solution retenue.
 */



add_action( 'admin_menu', 'sator_admin_menu' );

if ( ! function_exists( 'sator_admin_menu' ) ) :
    function sator_admin_menu() {
        add_options_page( __('Réglages de Sator', 'sator' ), __('Sator', 'sator' ), 'manage_options', 'sator_plugin', 'sator_options_page' );
    }
endif;

if ( ! function_exists( 'sator_admin_init' ) ) :
    function sator_admin_init() {

        global $defineSettings;

        add_option( 'sator_settings', $defineSettings );

        register_setting( 'sator_settings_group', 'sator_settings' );
        
        add_settings_section( 'message', __( 'Merci de saisir des données correctes afin de ne pas rendre le site inutilisable. :)', 'sator' ), 'sator_section_caution_callback', 'sator_plugin' );
        add_settings_section( 'section-1', __( 'Administrateur', 'sator' ), 'sator_section1_callback', 'sator_plugin' );
        add_settings_section( 'comments-1', __( 'Les clefs de reCAPTCHA sont indiquées sur les pages <a href=" https://www.google.com/recaptcha/admin" target="blank">reCaptcha, facile pour les humains, difficile pour les robots</a>.', 'sator' ), 'sator_section1_comments_callback', 'sator_plugin' );
        add_settings_section( 'section-2', __( 'Éditeur', 'sator' ), 'sator_section2_callback', 'sator_plugin' );
        add_settings_section( 'comments-2', __( 'Les données géographiques permettent la localisation de l’entreprise. Elles ne doivent pas être confondues avec les informations légales sur l’éditeur.', 'sator' ), 'sator_section2_comments_callback', 'sator_plugin' );
        add_settings_field( 'developpmentStatus', __( 'En développement', 'sator' ), 'sator_developpment_status_callback', 'sator_plugin', 'section-1' );
        add_settings_field( 'administratorIdentity', __( 'Identité de l’administrateur', 'sator' ), 'sator_administrator_identity_callback', 'sator_plugin', 'section-1' );
        add_settings_field( 'administratorEmail', __( 'Adresse électronique de l’administrateur', 'sator' ), 'sator_administrator_email_callback', 'sator_plugin', 'section-1' );
        add_settings_field( 'keyCaptchaPublic', __( 'Clef publique de reCAPTCHA', 'sator' ), 'sator_key_captcha_public_callback', 'sator_plugin', 'section-1' );
        add_settings_field( 'keyCaptchaPrivate', __( 'Clef secrète de reCAPTCHA', 'sator' ), 'sator_key_captcha_private_callback', 'sator_plugin', 'section-1' );
        add_settings_field( 'keyGoogleMaps', __( 'Clef Google Maps', 'sator' ), 'sator_key_google_maps', 'sator_plugin', 'section-1' );
        add_settings_field( 'idGoogleAnalytics', __( 'Code de suivi de Google Analytics', 'sator' ), 'sator_id_google_analytics', 'sator_plugin', 'section-1' );
        add_settings_field( 'organizationName', __( 'Nom de l’organisation', 'sator' ), 'sator_organization_name_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'editorIdentity', __( 'Identité de l’éditeur', 'sator' ), 'sator_editor_identity_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'editorEmail', __( 'Adresse électronique de l’éditeur', 'sator' ), 'sator_editor_email_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'editorAddress', __( 'Adresse de l’éditeur', 'sator' ), 'sator_editor_address_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'editorZip', __( 'Code postal de l’éditeur', 'sator' ), 'sator_editor_zip_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'editorCity', __( 'Localité de l’éditeur', 'sator' ), 'sator_editor_city_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'editorCountry', __( 'Pays de l’éditeur', 'sator' ), 'sator_editor_country_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'hostName', __( 'Nom de l’hébergeur', 'sator' ), 'sator_host_name_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'hostAddress', __( 'Adresse de l’hébergeur', 'sator' ), 'sator_host_address_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'hostZip', __( 'Code postal de l’hébergeur', 'sator' ), 'sator_host_zip_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'hostCity', __( 'Ville de l’hébergeur', 'sator' ), 'sator_host_city_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'hostCountry', __( 'Pays de l’hébergeur', 'sator' ), 'sator_host_country_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'mappingRegion', __( 'Pays de l’entreprise', 'sator' ), 'sator_mapping_region_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'mappingPlacename', __( 'Localité de l’entreprise', 'sator' ), 'sator_mapping_placename_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'mappingLatitude', __( 'Latitude de l’entreprise', 'sator' ), 'sator_latitude_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'mappingLongitude', __( 'Longitude de l’entreprise', 'sator' ), 'sator_mapping_longitude_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'mappingZoom', __( 'Zoom de la carte en provenance de Google Maps', 'sator' ), 'sator_mapping_zoom_callback', 'sator_plugin', 'section-2' );
        add_settings_field( 'creditsText', __( 'Crédits et licences (au format Html)', 'sator' ), 'sator_credits_text_callback', 'sator_plugin', 'section-2' );
    }
endif;
add_action( 'admin_init', 'sator_admin_init' );

if ( ! function_exists( 'sator_options_page' ) ) :
    function sator_options_page() {
    ?>
      <div class="wrap">
          <h2><?php _e( 'Réglages du thème et des extensions Sator', 'sator' );?></h2>
          <form action="options.php" method="post">
            <?php settings_fields( 'sator_settings_group' );?>
            <?php do_settings_sections( 'sator_plugin' );?>
            <?php submit_button();?>
          </form>
      </div>
    <?php }
endif;

if ( ! function_exists( 'sator_section_caution_callback' ) ) :
    function sator_section_caution_callback() {
    }
endif;

if ( ! function_exists( 'sator_section1_callback' ) ) :
    function sator_section1_callback() {
        esc_html_e( 'Informations techniques', 'sator' );
    }
endif;

if ( ! function_exists( 'sator_section1_comments_callback' ) ) :
    function sator_section1_comments_callback() {
    }
endif;

if ( ! function_exists( 'sator_section2_callback' ) ) :
    function sator_section2_callback() {
        esc_html_e( 'Informations légales', 'sator' );
    }
endif;

if ( ! function_exists( 'sator_section2_comments_callback' ) ) :
    function sator_section2_comments_callback() { 
    }
endif;

if ( ! function_exists( 'sator_developpment_status_callback' ) ) :
    function sator_developpment_status_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'developpmentStatus';
        $value = esc_attr( $settings[$field] );
        echo '<input type="checkbox" name="sator_settings['.$field.']" value="1" '.checked($value, 1, false).' />'."\n";
    }
endif;

if ( ! function_exists( 'sator_administrator_identity_callback' ) ) :
    function sator_administrator_identity_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'administratorIdentity';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_administrator_email_callback' ) ) :
    function sator_administrator_email_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'administratorEmail';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_key_captcha_public_callback' ) ) :
    function sator_key_captcha_public_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'keyCaptchaPublic';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_key_captcha_private_callback' ) ) :
    function sator_key_captcha_private_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'keyCaptchaPrivate';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_key_google_maps' ) ) :
    function sator_key_google_maps() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'keyGoogleMaps';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />' ."\n";
    }
endif;

if ( ! function_exists( 'sator_id_google_analytics' ) ) :
    function sator_id_google_analytics() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'idGoogleAnalytics';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />' ."\n";
    }
endif;


if ( ! function_exists( 'sator_organization_name_callback' ) ) :
    function sator_organization_name_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'organizationName';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_editor_identity_callback' ) ) :
    function sator_editor_identity_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'editorIdentity';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_editor_email_callback' ) ) :
    function sator_editor_email_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'editorEmail';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_editor_address_callback' ) ) :
	function sator_editor_address_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'editorAddress';
        $value = esc_attr( $settings[$field] );
        echo '<textarea name="sator_settings['.$field.']" rows="7" cols="50">'.$value.'</textarea>'."\n";
    }
endif;

if ( ! function_exists( 'sator_editor_zip_callback' ) ) :
    function sator_editor_zip_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'editorZip';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_editor_city_callback' ) ) :
    function sator_editor_city_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'editorCity';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_editor_country_callback' ) ) :
    function sator_editor_country_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'editorCountry';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_host_name_callback' ) ) :
    function sator_host_name_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'hostName';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_host_address_callback' ) ) :
    function sator_host_address_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'hostAddress';
        $value = esc_attr( $settings[$field] );
        echo '<textarea name="sator_settings['.$field.']" rows="7" cols="50">'.$value.'</textarea>'."\n";
    }
endif;

if ( ! function_exists( 'sator_host_zip_callback' ) ) :
    function sator_host_zip_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'hostZip';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="10" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_host_city_callback' ) ) :
    function sator_host_city_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'hostCity';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_host_country_callback' ) ) :
    function sator_host_country_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'hostCountry';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_mapping_region_callback' ) ) :
    function sator_mapping_region_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'mappingRegion';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_mapping_placename_callback' ) ) :
    function sator_mapping_placename_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'mappingPlacename';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_latitude_callback' ) ) :
    function sator_latitude_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'mappingLatitude';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />'."\n";
    }
endif;

if ( ! function_exists( 'sator_mapping_longitude_callback' ) ) :
    function sator_mapping_longitude_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'mappingLongitude';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="50" />' ."\n";
    }
endif;

if ( ! function_exists( 'sator_mapping_zoom_callback' ) ) :
    function sator_mapping_zoom_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'mappingZoom';
        $value = esc_attr( $settings[$field] );
        echo '<input type="text" name="sator_settings['.$field.']" value="'.$value.'" size="2" />' ."\n";
    }
endif;

if ( ! function_exists( 'sator_credits_text_callback' ) ) :
    function sator_credits_text_callback() {
        $settings = (array) get_option( 'sator_settings' );
        $field = 'creditsText';
        $value = esc_attr( $settings[$field] );
        echo '<textarea name="sator_settings['.$field.']" rows="25" cols="50">'.$value.'</textarea>'."\n";
    }
endif;