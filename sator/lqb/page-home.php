<?php
/* Template Name: Home */ 
?>
<?php
$pictures_flat_a = [];

for ( $i = 1; $i <= 12; $i++ ) {

    $pictures_flat_a[] = 'flat-a-'.$i;
}

$rand = array_rand( $pictures_flat_a, 2 );
$src_flat_a_a = $pictures_flat_a[$rand[0]];
$src_flat_a_b = $pictures_flat_a[$rand[1]];

$pictures_flat_b = [];

for ( $i = 1; $i <= 11; $i++ ) {

    $pictures_flat_b[] = 'flat-b-'.$i;

}

$rand = array_rand( $pictures_flat_b, 2 );
$src_flat_b_a = $pictures_flat_b[$rand[0]];
$src_flat_b_b = $pictures_flat_b[$rand[1]];

$pictures_flat_c = [];

for ( $i = 1; $i <= 9; $i++ ) {

    $pictures_flat_c[] = 'flat-c-'.$i;

}

$rand = array_rand( $pictures_flat_c, 2 );
$src_flat_c_a = $pictures_flat_c[$rand[0]];
$src_flat_c_b = $pictures_flat_c[$rand[1]];
?>
<?php
get_header();?>

    <div id="primary" class="site__content__primary">
        <main id="main" class="entry">

            <?php
            while ( have_posts() ) : the_post();
            ?>

            <article id="post-<?php the_ID();?>" <?php post_class('entry__post');?>>

                <div class="entry__post__content">
                    <?php
                        the_content();
                    ?>

                   <section class="entry__post__content__home">
                        <figure>
                            <picture><!-- https://stackoverflow.com/questions/12899691/use-of-picture-inside-figure-element-in-html5 -->
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/place-4-1920.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/place-4-2560.jpg 2x" 
                                    media="(min-width: 160em)">
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/place-4-1280.jpg 1x, <?php echo get_stylesheet_directory_uri();?>/library/img/place-4-1920.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/place-4-2560.jpg 3x">
                                <img class="entry__post__content__home__feature" src="<?php echo get_stylesheet_directory_uri();?>/library/img/place-4-320.jpg" alt="Place de Stalingrad, vue depuis notre immeuble" title="Place de Stalingrad, vue depuis notre immeuble">
                            </picture>
                            <figcaption class="tac">Place de Stalingrad, vue depuis notre immeuble</figcaption>
                        </figure>
                        <p>Orlane et Marisol vous accueillent dans un hôtel particulier datant du début du XIXème siècle situé place de Stalingrad à Bordeaux. Un superbe et large escalier arrondi en pierres permet de gravir les étages. La vue est imprenable et exceptionnelle sur les quais de Bordeaux et ses prestigieuses façades.</p>
                        <p>Le quartier comprend des commerces, un <a href="http://www.carrefour.fr/magasin/market-bordeaux-bastide" target="_blank" title="Aller sur la page de Carrefour Market Bordeaux Bastide (S’ouvre dans une nouvelle fenêtre)">supermarché</a>, des restaurants, des cafés et un <a href="http://bordeaux.megarama.fr/" target="_blank" title="Aller sur la page du Cinéma Mégarama (S’ouvre dans une nouvelle fenêtre)">complexe cinématographique</a>.</p>
                        <p>Trois appartements sont proposés à la location par l’intermédiaire d’<a href="https://www.airbnb.fr/" rel="nofollow" target="_blank" title="Aller sur le site Airbnb (S’ouvre dans une nouvelle fenêtre)">Airbnb</a>.</p>
                        <p>Orlane et Marisol veillent à vous assurer un séjour agréable dans leur logis.</p>
                        <h3>Premier appartement</h3>
                        <div class="entry__post__content__home__gallery entry__post__content__home__gallery--odd">
                            <picture>
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_a;?>-1280.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_a;?>-1920.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_a;?>-2560.jpg 3x" 
                                    media="(min-width: 160em)">
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_a;?>-640.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_a;?>-1280.jpg 2x, 
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_a;?>-1920.jpg 3x">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_a;?>-320.jpg" alt="Appartement" title="Appartement">
                            </picture>
                            <picture>
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_b;?>-1280.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_b;?>-1920.jpg 2x, 
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_b;?>-2560.jpg 3x"
                                    media="(min-width: 160em)">
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_b;?>-640.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_b;?>-1280.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_b;?>-1920.jpg 3x">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_a_b;?>-320.jpg" alt="Appartement" title="Appartement">
                            </picture>
                        </div><!-- .entry__post__content__home__gallery -->
                        <p>La capacité est de trois personnes, le studio comprend deux lits.</p>
                        <div class="entry__post__content__home__visit entry__post__content__home__visit--odd">
                            <div>
                                <p>Voir plus d’<a href="<?php echo get_stylesheet_directory_uri();?>/flat-a" title="Voir plus d’images de cet appartement">images</a> de cet appartement…</p>
                            </div>
                            <div class="entry__post__content__home__visit__icon legacy-dn" onclick="window.location.href='<?php echo get_stylesheet_directory_uri();?>/flat-a'; return false;"></div>
                        </div><!-- .entry__post__content__home__visit -->
                        <h3>Deuxième appartement</h3>
                        <div class="entry__post__content__home__gallery entry__post__content__home__gallery--even">
                            <picture>
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_a;?>-1280.jpg 1x, 
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_a;?>-1920.jpg 2x, 
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_a;?>-2560.jpg 3x"
                                    media="(min-width: 160em)">
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_a;?>-640.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_a;?>-1280.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_a;?>-1920.jpg 3x">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_a;?>-320.jpg" alt="Appartement" title="Appartement">
                            </picture>
                            <picture>
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_b;?>-1280.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_b;?>-1920.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_b;?>-2560.jpg 3x"
                                    media="(min-width: 160em)">
                                <source srcset="
                                        <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_b;?>-640.jpg 1x,
                                        <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_b;?>-1280.jpg 2x,
                                        <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_b;?>-1920.jpg 3x">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_b_b;?>-320.jpg" alt="Appartement" title="Appartement">
                            </picture>
                        </div><!-- .entry__post__content__home__gallery -->
                        <p>La capacité est de deux personnes, le studio comprend un lit.</p>
                        <div class="entry__post__content__home__visit entry__post__content__home__visit--even">
                            <div>
                                <p>Voir plus d’<a href="<?php echo get_stylesheet_directory_uri();?>/flat-b" title="Voir plus d’images de cet appartement">images</a> de cet appartement…</p>
                            </div>
                            <div class="entry__post__content__home__visit__icon legacy-dn" onclick="window.location.href='<?php echo get_stylesheet_directory_uri();?>/flat-b'; return false;"></div>
                        </div><!-- .entry__post__content__home__visit -->
                        <h3>Troisième appartement</h3>
                        <div class="entry__post__content__home__gallery entry__post__content__home__gallery--odd">
                            <picture>
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_a;?>-1280.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_a;?>-1920.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_a;?>-2560.jpg 3x"
                                    media="(min-width: 160em)">
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_a;?>-640.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_a;?>-1280.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_a;?>-1920.jpg 3x">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_a;?>-320.jpg" alt="Appartement" title="Appartement">
                            </picture>
                            <picture>
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_b;?>-1280.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_b;?>-1920.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_b;?>-2560.jpg 3x" 
                                    media="(min-width: 160em)">
                                <source srcset="
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_b;?>-640.jpg 1x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_b;?>-1280.jpg 2x,
                                    <?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_b;?>-1920.jpg 3x">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/library/img/<?php echo $src_flat_c_b;?>-320.jpg" alt="Appartement" title="Appartement">
                            </picture>
                        </div><!-- .entry__post__content__home__gallery -->
                        <p>La capacité est de quatre personnes, le studio comprend deux grands lits.</p>
                        <div class="entry__post__content__home__visit entry__post__content__home__visit--odd">
                            <div>
                                <p>Voir plus d’<a href="<?php echo get_stylesheet_directory_uri();?>/flat-c" title="Voir plus d’images de cet appartement">images</a> de cet appartement…</p>
                            </div>
                            <div class="entry__post__content__home__visit__icon legacy-dn" onclick="window.location.href='<?php echo get_stylesheet_directory_uri();?>/flat-c'; return false;"></div>
                        </div><!-- .entry__post__content__home__visit -->
                        <h3>Desserte</h3>
                        <div class="entry__post__content__home__travel">
                            <h4>Transports urbains</h4>
                            <div class="entry__post__content__home__travel__local">
                                <div class="legacy-dn">
                                    <svg>
                                        <use xlink:href="#tram"></use>
                                    </svg>
                                    <svg>
                                        <use xlink:href="#directions-boat"></use>
                                    </svg>
                                </div>
                                <div>
                                    <p>Nos appartements sont situés à quelques mètres de la station <em>Stalingrad</em> desservie par la ligne <em>A</em> du tramway et par les autobus. À proximité, un service de navette sur la Garonne, Bat3 (BatCub), est disponible. La place de Stalingrad comprend aussi un service de bicyclettes en libre-service, <a href="https://www.vcub.com/" target="_blank" title="Aller sur le site de Vcub (S’ouvre dans une nouvelle fenêtre)">V3</a>. Toutes les informations à propos des transports urbains sont accessibles sur <a href="https://www.infotbm.com/" target="_blank" title="Aller sur le site de Transports Bordeaux Métropole (S’ouvre dans une nouvelle fenêtre)">InfoTBM</a>.</p>
                                </div>
                                <div class="legacy-dn">
                                    <svg>
                                        <use xlink:href="#bus"></use>
                                    </svg>
                                    <svg>
                                        <use xlink:href="#directions-bike"></use>
                                    </svg>
                                </div>
                            </div><!-- .entry__post__content__home__travel__local -->
                            <h4>Accès en train et par avion</h4>
                            <div class="entry__post__content__home__travel__national">
                                    <div class="legacy-dn">
                                        <svg>
                                            <use xlink:href="#train"></use>
                                        </svg>
                                    </div>
                                    <div>
                                        <p>La gare de <a href="https://www.gares-sncf.com/fr/gare/frboj/bordeaux-saint-jean" target="blank" title="Aller sur le site de la gare de Bordeaux Saint-Jean (S’ouvre dans une nouvelle fenêtre)">Bordeaux Saint-Jean</a> est située à dix minutes en tramway de nos appartements. Grâce au <abbr title="Train à Grande Vitesse">Tgv</abbr>, la <a href="http://www.sncf.com/" title="Aller sur le site de Sncf (S’ouvre dans une nouvelle fenêtre)">Sncf</a> relie Paris à Bordeaux en 2 h 04. Le <a href="https://www.ter.sncf.com/aquitaine" target="blank" title="Aller sur le site de Ter Nouvelle-Aquitaine (S’ouvre dans une nouvelle fenêtre)">Ter Nouvelle-Aquitaine</a> propose de nombreuses dessertes. L’<a href="http://www.bordeaux.aeroport.fr/" target="blank" title="Aller sur le site de l’Aéroport de Bordeaux-Mérignac (S’ouvre dans une nouvelle fenêtre)">Aéroport de Bordeaux</a> est distant de 21 kilomètres.</p>
                                    </div>
                                    <div class="legacy-dn">
                                        <svg class="h-w-medium fill-dark-primary-color">
                                            <use xlink:href="#flight"></use>
                                        </svg>
                                    </div>
                            </div><!-- .entry__post__content__home__travel__national -->
                            <h4>Accès par la route</h4>
                            <div class="entry__post__content__home__travel__road">
                                    <div class="legacy-dn">
                                        <svg>
                                            <use xlink:href="#directions-car"></use>
                                        </svg>
                                    </div>
                                    <div>
                                        <p>La rocade A630 se situe à cinq kilomètres de notre logis. Les plages sont à une heure et les Pyrénées à deux heures trente.</p>
                                        <p>Nous proposons un parking privé à l’arrière de l’immeuble au prix de 12 euros par jour.</p>
                                    </div>
                            </div><!-- .entry__post__content__home__travel_road -->
                        </div><!-- .entry__post__content__home__travel -->
                        <h3>Situation géographique</h3>
                        <?php echo do_shortcode('[GoogleMaps]');?>
                    </section><!-- .entry__post__content__home -->

                </div><!-- .entry__post__content -->

                <?php if ( get_edit_post_link() ) : ?>
                    <footer class="entry__post__footer">
                        <?php
                            edit_post_link(
                                sprintf(
                                    wp_kses(
                                        __( 'Modifier <span class="screen-reader-text">%s</span>', 'sator' ),
                                        array(
                                            'span' => array(
                                                'class' => array(),
                                            ),
                                        )
                                    ),
                                    get_the_title()
                                ),
                                '<span class="entry__post__footer__edit-link">',
                                '</span>'
                            );
                        ?>
                    </footer><!-- .entry__post__footer -->
                <?php endif;?>
            </article><!-- #post-<?php the_ID();?> -->

            <?php
            endwhile;?>

        </main><!-- #main .entry -->
    </div><!-- #primary .site__content__primary -->

<?php
get_sidebar();
get_footer();