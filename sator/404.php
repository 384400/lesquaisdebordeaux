<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package sator
 */

get_header();?>

    <div id="primary" class="site__content__primary">
        <main id="main" class="entry">

            <section class="entry__section entry__section--404 entry__section--not-found">
                <header class="entry__header">
                    <h1 class="entry__title"><?php esc_html_e( 'Erreur', 'sator' );?></h1>
                </header><!-- .entry__header -->

                <div class="entry__content">
                    <p><?php esc_html_e( 'Le contenu demandé n’existe pas. Les liens-ci-dessous ou une recherche pourrait vous être utile.', 'sator' );?></p>

                    <?php
                        get_search_form();

                        the_widget( 'WP_Widget_Recent_Posts' );
                    ?>

                    <div class="widget widget_categories">
                        <h2 class="widget-title"><?php esc_html_e( 'Catégories les plus fréquentes', 'sator' );?></h2>
                        <ul>
                        <?php
                            wp_list_categories( array(
                                'orderby'    => 'count',
                                'order'      => 'DESC',
                                'show_count' => 1,
                                'title_li'   => '',
                                'number'     => 10,
                            ) );
                        ?>
                        </ul>
                    </div><!-- .widget -->

                    <?php

                        $archive_content = '<p>' . sprintf( esc_html__( 'Consultez les archives mensuelles.', 'sator' ) ) . '</p>';
                        the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );

                        the_widget( 'WP_Widget_Tag_Cloud' );
                    ?>

                </div><!-- .entry__content -->
            </section><!-- .entry__section -->

        </main><!-- #main .entry -->
    </div><!-- #primary .site__content__primary -->

<?php
get_footer();