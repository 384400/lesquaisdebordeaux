<?php
/**
 * Plugin Name: Sator Galleries
 * Plugin URI: http://www.384400.xyz
 * Description: Add galleries to pages [DisplayGalleryPage]. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

/**
 * Nous creons les champs personnalises.
 */

if ( ! function_exists( 'sator_create_gallery_tags' ) ) :
    function sator_create_gallery_tags( $post_id ) {

        add_post_meta( $post_id, 'gallery_path', '', true );

        add_post_meta( $post_id, 'gallery_prefix', '', true );
    }
endif;
add_action( 'wp_insert_post', 'sator_create_gallery_tags', 1 );

/**
 * Nous affichons les champs personnalises.
 */

if ( ! function_exists( 'sator_display_gallery_page' ) ) :
    function sator_display_gallery_page() {

        global $post;

        $galleryPath = get_post_meta( $post->ID, 'gallery_path', true );
        $galleryPrefix = get_post_meta( $post->ID, 'gallery_prefix', true );
    
        if ( $galleryPath && $galleryPrefix ) {
            $outputGallery = null;

            $path = get_stylesheet_directory().'/'.$galleryPath.'/'.$galleryPrefix.'*-320.jpg';

            $pictures = glob( $path );

            $itemsGallery = [];

            foreach ( $pictures as $picture ) {
                $itemsGallery[] = str_replace( get_stylesheet_directory(), get_stylesheet_directory_uri(), $picture );
            }

            natsort( $itemsGallery );
            /* Un tableau est necessaire pour ordonner les elements : "10" doit être apres "9", pas apres "1" ! */

            $outputGallery .= '<div class="entry__post__content__flat__gallery">';
            foreach ( $itemsGallery as $itemGallery ) {
                $outputGallery .= '<details>';
                $outputGallery .= '<summary>';
                $outputGallery .= '<picture>';
                $outputGallery .= '<source srcset="';
                $outputGallery .= str_replace( '320', '1280', $itemGallery ).' 1x,';
                $outputGallery .= str_replace( '320', '1920', $itemGallery ).' 2x,';
                $outputGallery .= str_replace( '320', '2560', $itemGallery ).' 3x"';
                $outputGallery .= "\t\t\t\t\t".'media="(min-width: 160em)" />';
                $outputGallery .= '<source srcset="';
                $outputGallery .= str_replace( '320', '640', $itemGallery ).' 1x,';
                $outputGallery .= str_replace( '320', '1280', $itemGallery ).' 2x,';
                $outputGallery .= str_replace( '320', '1920', $itemGallery ).' 3x" />';
                $outputGallery .= '<img src="'.$itemGallery.'" alt="" title="" />';
                $outputGallery .= '</picture>';
                $outputGallery .= '</summary>';
                $outputGallery .= '<div class="entry__post__content__flat__gallery__lightbox">';
                $outputGallery .= '<img srcset="';
                $outputGallery .= str_replace( '320', '640', $itemGallery ).' 640w,';
                $outputGallery .= str_replace( '320', '1280', $itemGallery ).' 1280w,';
                $outputGallery .= str_replace( '320', '1920', $itemGallery ).' 1920w,';
                $outputGallery .= str_replace( '320', '2560', $itemGallery ).' 2560w"';
                $outputGallery .= ' ';
                $outputGallery .= 'src="'.$itemGallery.'"';
                $outputGallery .= ' ';
                $outputGallery .= 'sizes="100vw" alt="" title="" />';
                $outputGallery .= '</div><!-- .entry__post__content__flat__gallery__lightbox -->';
                $outputGallery .= '</details>';
            }
            $outputGallery .= '</div><!-- .entry__post__content__flat__gallery -->';
            return $outputGallery;
        }
    }
endif;
add_shortcode( 'DisplayGalleryPage', 'sator_display_gallery_page' );