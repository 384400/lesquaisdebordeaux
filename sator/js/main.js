/* http://jshint.com/ */
console.group('%cCredits', 'color: #ffa000; font-family: monospace; font-size: xx-large;');
console.group('%cDeveloppment', 'color: #607d8b; font-family: monospace; font-size: x-large;');
console.info('%c\u2641' + ' ' + '%c384400' + ' ' + '%c\u263d', 'color: #607d8b; font-family: monospace; font-size: large;', 'color: #111111; font-family: monospace; font-size: large;', 'color: #ffa000; font-family: monospace; font-size: large;');
console.groupEnd();
console.group('%cTheme', 'color: #607d8b; font-family: monospace; font-size: x-large;');
console.table([['S', 'A', 'T', 'O', 'R'], ['A', 'R', 'E', 'P', 'O'], ['T', 'E', 'N', 'E', 'T'], ['O', 'P', 'E', 'R', 'A'], ['R', 'O', 'T', 'A', 'S']]);
console.groupEnd();
console.groupEnd();
var Modernizr;
document.querySelector('html').classList.remove('sr-nojs');
document.querySelector('html').classList.add('sr-js');
if (Modernizr.cssremunit && Modernizr.cssvhunit && Modernizr.details && Modernizr.flexbox && Modernizr.flexwrap && Modernizr.inlinesvg) {
    document.querySelector('html').classList.remove('sr-js-legacy');
    document.querySelector('html').classList.add('sr-js-trendy');
} else {
    if (document.cookie.replace(/(?:(?:^|.*;\s*)legacy\s*\=\s*([^;]*).*$)|^.*$/, '$1') === 'true') {
        document.querySelector('#outdated').classList.add('dn');
    }
}
var listNoSupport = document.querySelectorAll('.no-support-js');
for (var i = 0; i < listNoSupport.length; i++) {
     listNoSupport[i].classList.add('dn');
}
var listSupport = document.querySelectorAll('.support-js');
for (var i = 0; i < listNoSupport.length; i++) {
     listSupport[i].classList.remove('legacy-dn');
     listSupport[i].classList.add('legacy-db');
}
function closeAlertLegacy() {
    document.querySelector('#outdated').classList.add('dn');
    var now = new Date();
    now.setTime(now.getTime() + 1 * 3600 * 1000);
    document.cookie = 'legacy=true;expires=' + now.toUTCString() + ';path=/';
}
var listAcronyms = document.querySelectorAll('abbr');
var titleAcronyms = [];
for (var i = 0; i < listAcronyms.length; i++) {
    if (!titleAcronyms.includes(listAcronyms[i].title)) {
        listAcronyms[i].classList.add('first-acronym');
    }
}
var attributeContentAcronyms = document.querySelectorAll('.first-acronym');
var title;
for (var i = 0; i < attributeContentAcronyms.length; i++) {
    title = attributeContentAcronyms[i].title;
}