<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sator
 */

?>

    </div><!-- #content site__content -->

    <footer id="colophon" class="site__footer">
        <div class="footer__info">
            <p><a href="<?php echo esc_url( __( 'https://wordpress.org/', 'sator' ) ); ?>"><?php
                /* translators: %s: CMS name, i.e. WordPress. */
                printf( esc_html__( 'Propulsé par %s', 'sator' ), 'WordPress' );
            ?></a>
            <span class="footer__info__sep"> | </span>
            <?php
                /* translators: 1: Theme name, 2: Theme author. */
                printf( esc_html__( 'Thème&#x202f;: %1$s par %2$s.', 'sator' ), 'sator', '<a href="http://www.384400.xyz/">384400</a>' );
            ?></p>
        </div><!-- .footer__info -->
    </footer><!-- #colophon .site__footer -->
</div><!-- #page .site -->

<?php wp_footer(); ?>

</body>
</html>