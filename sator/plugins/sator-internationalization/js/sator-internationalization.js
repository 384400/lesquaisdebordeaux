var lang_objects;
var defaultLang = lang_objects.defaultLang;
var userLang = lang_objects.userLang;
if (userLang != defaultLang) {
    document.querySelector('html').classList.remove('sr-user-lang-' + defaultLang);
    document.querySelector('html').classList.add('sr-user-lang-' + userLang);
}