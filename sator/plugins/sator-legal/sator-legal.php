<?php
/**
 * Plugin Name: Sator Legal
 * Plugin URI: http://www.384400.xyz
 * Description: Manage imprint [ImprintPageLang] and cookie notice in accordance. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

$optionsSator = get_option( 'sator_settings' );

/**
 * Nous affichons la page des mentions legales qui sera appelee avec le Shortcode [ImprintPageFr].
 */

if ( ! function_exists( 'sator_create_imprint_page_fr' ) ) :
    function sator_create_imprint_page_fr() {

        global $optionsSator;

        $siteUrl = get_bloginfo( 'url');
        $siteName = get_bloginfo( 'name' );

        if ( $optionsSator['editorEmail'] ) {
            $editorEmail = $optionsSator['editorEmail'];
        }

        if ( $optionsSator['creditsText'] ) {
            $displayCredits = $optionsSator['creditsText'];

        } else {
            $displayCredits = '<p>Aucune licence n’est à attribuer à des tiers.</p>';
        }
        
        $editorEmailLink = '<a href="mailto:'.$editorEmail.'" title="Envoyer un mail à '.$editorEmail.'">'.$editorEmail.'</a>';
        $siteLink = '<a href="'.$siteUrl.'" title="Aller sur le site '.$siteName.' (S’ouvre dans une nouvelle fenêtre)" target="_blank" rel="nofollow">'.$siteName.'</a>';

        $displayEditor = null;
        $displayAdmin = null;
        $displayHost = null;
        $publishDate = null;

        $displayEditor .= '<ul class="list-ml0 list-n">';

        if ( $optionsSator['organizationName'] && $optionsSator['editorIdentity']  && $optionsSator['editorAddress'] && $optionsSator['editorZip'] && $optionsSator['editorCity']  && $optionsSator['editorCountry'] && $optionsSator['editorEmail'] ) {
            $displayEditor .= "\t".'<li>'.$optionsSator['organizationName'].'&#x00A0;<strong>&#x2192;</strong>&#x00A0;'.$optionsSator['editorIdentity'].'</li>';
            $displayEditor .= '<li>';
            $displayEditor .= '<address>';
            $displayEditor .= '<ul>';
            $displayEditor .= '<li>'.$optionsSator['editorAddress'].'</li>';
            $displayEditor .= '<li>'.$optionsSator['editorZip'].' '.$optionsSator['editorCity'].'</li>';
            $displayEditor .= '<li>'.$optionsSator['editorCountry'].'</li>';
            $displayEditor .= '<li>'.'<a href="mailto:'.$editorEmail.'" title="Envoyer un mail à '.$editorEmail.'">'.$editorEmail.'</a></li>';
            $displayEditor .= '</ul>';
            $displayEditor .= '</address>';
            $displayEditor .= '</li>';
        } else {
            $displayEditor .= "\t".'<li><em>Informations à renseigner.</em></li>';
        }
        $displayEditor .= '</ul>';

        $displayAdmin .= '<ul class="list-ml0 list-n">';
        if ( $optionsSator['administratorIdentity'] && $optionsSator['administratorEmail'] ) {
            $displayAdmin .= '<li>'.$optionsSator['administratorIdentity'].'</li>';
            $displayAdmin .= '<li>'.'<a href="mailto:'.$optionsSator['administratorEmail'].'" title="Envoyer un mail à '.$optionsSator['administratorEmail'].'">'.$optionsSator['administratorEmail'].'</a></li>';
        } else {
            $displayAdmin .= '<li><em>Informations à renseigner.</em></li>';
        }
        $displayAdmin .= '</ul>';

        $displayHost .= '<ul class="list-ml0 list-n">';
        if ( $optionsSator['hostName'] && $optionsSator['hostAddress'] && $optionsSator['hostZip'] && $optionsSator['hostCity']  && $optionsSator['hostCountry'] ) {
            $displayHost .= '<li>'.$optionsSator['hostName'].'</li>';
            $displayHost .= '<li>';
            $displayHost .= '<address>';
            $displayHost .= '<ul>';
            $displayHost .= '<li>'.$optionsSator['hostAddress'].'</li>';
            $displayHost .= '<li>'.$optionsSator['hostZip'].' '.$optionsSator['hostCity'].'</li>';
            $displayHost .= '<li>'.$optionsSator['hostCountry'].'</li>';
            $displayHost .= '</ul>';
            $displayHost .= '</address>';
            $displayHost .= '</li>';
        } else {
            $displayHost .= '<p>'.$siteName.' est auto-hébergé.</p>';
        }
        $displayHost .= '</ul>';
        
        $publishDate = get_the_modified_date('d-m-Y');

        $contentImprint = <<<EOT
<h3>I. Présentation du site</h3>
<p>En vertu de l’article 6 de la loi no 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique, il est précisé aux utilisateurs du site $siteLink l’identité des différents intervenants dans le cadre de sa réalisation et de son suivi…</p>
<h4>Éditeur</h4>
{$displayEditor}
<h4>Administrateur</h4>
{$displayAdmin}
<h4>Hébergeur</h4>
{$displayHost}
<h3>II. Conditions générales d’utilisation du site et des services proposés</h3>
<p>L’utilisation du site {$siteLink} implique l’acceptation pleine et entière des conditions générales d’utilisation ci-après décrites.</p>
<p>Ce site est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par {$siteLink}, qui s’efforcera alors de communiquer préalablement aux utilisateurs les dates et heures de l’intervention.</p>
<p>Le site {$siteLink} a été mis à jour le {$publishDate} par {$optionsSator['administratorIdentity']}. De la même façon, les mentions légales peuvent être modifiées à tout moment&#x202f;: elles s’imposent néanmoins à l’utilisateur qui est invité à s’y référer le plus souvent possible afin d’en prendre connaissance.</p>
<h3>III. Description des services fournis</h3>
<p>Le site {$siteLink} a pour objet de fournir des informations concernant ses activités.</p>
<p>Le site {$siteLink} s’est efforcé de fournir sur le site des informations aussi précises que possible. Toutefois, il ne peut être tenu responsable des omissions, inexactitudes et carences dans la mise à jour, qu’elles soient de leur fait ou du fait des tiers partenaires qui leur fournissent ces informations.</p>
<p>Tous les informations indiquées sur le site {$siteLink} sont données à titre indicatif. Par ailleurs, les renseignements figurant sur le site {$siteLink} ne sont pas exhaustifs. Ils sont donnés sous réserve de modifications ayant été apportées depuis la mise en ligne.</p>
<h3>IV. Limitations contractuelles sur les données techniques</h3>
<p>Le site {$siteLink} utilise la technologie Javascript.</p>
<p>Le site {$siteLink} ne pourra être tenu responsable de dommages matériels liés à l’utilisation du site. De plus, l’utilisateur du site {$siteLink} s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis à jour.</p>
<h3>V. Propriété intellectuelle et contrefaçons</h3>
<p>Le site {$siteLink} s’est efforcé de recourir à des images libres de droit ou personnelles.</p>
{$displayCredits}
<p>Le site {$siteLink} est propriétaire des droits de propriété intellectuelle ou détient les droits d’usage sur les autres éléments accessibles sur le site, notamment textes, images, graphismes, logotypes, icônes, sons, logiciels.</p>
<p>Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable du site {$siteLink}.</p>
<p>Toute exploitation non autorisée du site ou de l’un quelconque des éléments qu’il contient sera considérée comme constitutive d’une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</p>
<h3>VI. Limitations de responsabilité</h3>
<p>Le site {$siteLink} ne pourra être tenu responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site $siteLink, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications indiquées au point IV, soit de l’apparition d’un bogue ou d’une incompatibilité.</p>
<p>Le site {$siteLink} ne pourra également être tenu responsable des dommages indirects (comme, par exemple, une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site.</p>
<p>Des espaces interactifs (possibilité de poser des questions dans l’espace contact) sont éventuellement à la disposition des utilisateurs. Les éditeurs du site {$siteLink} se réservent le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, les éditeurs du site {$siteLink} se réservent également la possibilité de mettre en cause la responsabilité civile ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamatoire, ou pornographique, quel que soit le support utilisé (texte, photographie…).</p>
<h3>VII. Gestion des données personnelles</h3>
<p>En France, les données personnelles sont notamment protégées par la loi no 78-87 du 6 janvier 1978, la loi no 2004-801 du 6 août 2004, l’article L. 226-13 du Code Pénal et la Directive Européenne du 24 octobre 1995.</p>
<p>À l’occasion de l’utilisation du site {$siteLink}, peuvent êtres recueillis&#x202f;:</p>
<ul>
<li>l’<abbr title="Uniform Ressouce Locator">Url</abbr> des liens par l’intermédiaire desquels l’utilisateur a accédé au site {$siteLink}&#x202f;;</li>
<li>le fournisseur d’accès de l’utilisateur&#x202f;;</li>
<li>l’adresse <abbr title="Internet Protocol">Ip</abbr> de l’utilisateur.</li>
</ul>
<p>En tout état de cause, les éditeurs du site {$siteLink} ne collectent des informations personnelles relatives à l’utilisateur que pour le besoin de certains services proposés par le site. L’utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu’il procède par lui-même à leur saisie. Il est alors précisé à l’utilisateur du site {$siteLink} l’obligation ou non de fournir ces informations.</p>
<p>Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, tout utilisateur dispose d’un droit d’accès, de rectification et d’opposition aux données personnelles le concernant, en effectuant sa demande écrite et signée, accompagnée d’une copie du titre d’identité avec signature du titulaire de la pièce, en précisant l’adresse à laquelle la réponse doit être envoyée.</p>
<p>Aucune information personnelle de l’utilisateur du site $siteLink n’est publiée à l’insu de l’utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers. Seule l’hypothèse du rachat du site et de ses droits permettrait la transmission des dites informations à l’éventuel acquéreur qui serait à son tour tenu de la même obligation de conservation et de modification des données vis-à-vis de l’utilisateur du site $siteLink.</p>
<p>Le site {$siteLink} n’est pas déclaré à la <a href="http://www.cnil.fr/" title="Aller sur le site de la Commission Nationale de l'Informatique et des Libertés (S’ouvre dans une nouvelle fenêtre)" target="_blank">Cnil</a>, car il ne recueille pas d’informations personnelles.</p>
<p>Les bases de données sont protégées par les dispositions de la loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases de données.</p>
<h3>VIII. Liens hypertextes et Cookies</h3>
<p>Le site {$siteLink} contient un certain nombre de liens hypertextes vers d’autres sites. Cependant, {$siteLink} n’a pas la possibilité de vérifier le contenu des sites ainsi visités, et n’assumera en conséquence aucune responsabilité de ce fait.</p>
<p>La navigation sur le site {$siteLink} est susceptible de provoquer l’installation de Cookie(s) sur l’ordinateur de l’utilisateur. Un Cookie est un fichier de petite taille qui ne permet pas l’identification de l’utilisateur, mais qui enregistre des informations relatives à la navigation d’un ordinateur sur un site. Les données ainsi obtenues visent à faciliter la navigation ultérieure sur le site, et ont également vocation à permettre diverses mesures de fréquentation.</p>
<p>Le refus d’installation d’un Cookie peut entraîner l’impossibilité d’accéder à certains services. L’utilisateur peut toutefois configurer son ordinateur afin de refuser l’installation des Cookies.</p>
<h3>IX. Droit applicable et attribution de juridiction</h3>
<p>Tout litige en relation avec l’utilisation du site {$siteLink} est soumis au droit français. Il pourra donner lieu à la saisie du tribunal compétent.</p>
<h3>X. Textes de références</h3>
<ul>
<li>Loi no 78-17 du 6 janvier 1978, notamment modifiée par la loi no 2004-801 du 6 août 2004 relative à l’informatique, aux fichiers et aux libertés.</li>
<li>Loi no 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique.</li>
</ul>
<h3>XI. Lexique</h3>
<dl>
<dt>Utilisateur</dt>
<dd>Internaute se connectant, utilisant le site susnommé.</dd>
<dt>Informations personnelles</dt>
<dd><em>Informations qui permettent, sous quelque forme que ce soit, directement ou non, l’identification des personnes physiques auxquelles elles s’appliquent</em> (article 4 de la loi no 78-17 du 6 janvier 1978).</dd>
</dl>
<p>Mentions légales mises à jour le <strong>{$publishDate}</strong>.</p>
EOT
;
        return $contentImprint;
    }
endif;
add_shortcode( 'ImprintPageFr', 'sator_create_imprint_page_fr' );

/**
 * Nous controlons l'existence du Cookie.
 */

if ( ! function_exists( 'sator_check_cookie_legal' ) ) :
    function sator_check_cookie_legal() {

        if ( ! isset ( $_COOKIE['cookie_policy'] ) ) {
            wp_enqueue_script( 'sator-fr-legal-js' );
        }
    }
endif;
add_action( 'wp_footer', 'sator_check_cookie_legal' );

/**
 * Nous emettons un avertissement et deposons le Cookie en JavaScript.
 */

if ( ! function_exists( 'sator_write_cookie_legal' ) ) :
    function sator_write_cookie_legal() {

        /**
         * Le script est appele dans la fonction principale.
         */
        
        global $optionsSator;

        $toLocalize = array(
            'noticeClose' => __( 'Fermer le message d’avertissement', 'sator' ),
            'noticeText' => __( 'En poursuivant votre navigation sur notre site, vous acceptez l’utilisation de traceurs pour réaliser des statistiques de visites.', 'sator' ),
            'noticeCheck' => __( '&#x2713;&#x00A0;D’accord.', 'sator' ),
            'cookieLifeTime' => '13'
        );
        
        
        if ( $optionsSator['developpmentStatus'] ) {
            wp_register_script( 'sator-fr-legal-js', plugins_url( 'js/sator-legal.js', __FILE__ ), '', '', true );
        } else {
            wp_register_script( 'sator-fr-legal-js', plugins_url( 'js/sator-legal-dist.js', __FILE__ ), '', '', true );
        }

        wp_localize_script( 'sator-fr-legal-js', 'notice_objects', $toLocalize );

    }
endif;
add_action( 'wp_enqueue_scripts', 'sator_write_cookie_legal' );