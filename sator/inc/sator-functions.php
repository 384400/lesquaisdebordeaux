<?php
if ( ! function_exists( 'sator_disable_emojicons_tinymce' ) ) :
    function sator_disable_emojicons_tinymce( $plugins ) {
        if ( is_array( $plugins ) ) { 
            return array_diff( $plugins, array( 'wpemoji' ) );
        } else {
            return array();
        }
    }
endif;
if ( ! function_exists( 'sator_disable_comments_status' ) ) :
    function sator_disable_comments_status() {
        return false;
    }
endif;
add_filter( 'comments_open', 'sator_disable_comments_status', 20, 2 );
add_filter( 'pings_open', 'sator_disable_comments_status', 20, 2 );

if ( ! function_exists( 'sator_disable_comments_hide_existing_comments' ) ) :
    function sator_disable_comments_hide_existing_comments( $comments ) {
        $comments = array();
        return $comments;
    }
endif;
add_filter( 'comments_array', 'sator_disable_comments_hide_existing_comments', 10, 2 );
if ( ! function_exists( 'sator_clean_output_admin_menu' ) ) :
    function sator_clean_output_admin_menu() { 
        remove_menu_page( 'edit-comments.php' );
    }
endif;
add_action( 'admin_menu', 'sator_clean_output_admin_menu' );
/*
 * https://gist.github.com/mattclements/eab5ef656b2f946c4bfb
 * https://dfactory.eu/wp-how-to-turn-off-disable-comments/
 */
if ( ! function_exists( 'sator_clean_output_admin_init' ) ) :
    function sator_clean_output_admin_init() {
        global $pagenow;

        if ( $pagenow === 'edit-comments.php' ) {  
            wp_redirect( admin_url() );
            exit;
        }

        $post_types = get_post_types();

        foreach ( $post_types as $post_type ) {
            if( post_type_supports( $post_type, 'comments' ) ) {
                remove_post_type_support( $post_type, 'comments' );

                remove_post_type_support( $post_type, 'trackbacks' );
            }
        }

        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    }
endif;
add_action( 'admin_init', 'sator_clean_output_admin_init' );

/*
 * https://wordpress.stackexchange.com/questions/190509/where-to-remove-link-from-comments-feed
 * http://crunchify.com/how-to-disable-auto-embed-script-for-wordpress-4-4-wp-embed-min-js/
 * http://www.limecanvas.com/how-to-remove-the-wordpress-generator-meta-tag/
 * https://wordpress.stackexchange.com/questions/190509/where-to-remove-link-from-comments-feed
 * https://codex.wordpress.org/Plugin_API/Filter_Reference/run_wptexturize
 * "Do not use this function before the 'init' action hook." ? !
 * https://codex.wordpress.org/Function_Reference/wptexturize
 */
if ( ! function_exists( 'sator_output_clean_init' ) ) :
    function sator_output_clean_init() {
        if ( ! is_admin() ) {
            wp_deregister_script( 'wp-embed' );

            wp_deregister_script( 'jquery' );
        }

        add_filter( 'emoji_svg_url', '__return_false' );

        add_filter( 'feed_links_show_comments_feed', '__return_false' );
        
        add_filter( 'run_wptexturize', '__return_false' );

        add_filter( 'tiny_mce_plugins', 'sator_disable_emojicons_tinymce' );

        remove_action( 'admin_print_styles', 'print_emoji_styles' );

        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );

        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

        remove_action( 'wp_head', 'wp_generator' );

        remove_action( 'wp_head', 'wp_oembed_add_host_js' );

        remove_action( 'wp_print_styles', 'print_emoji_styles' );

        remove_action( 'rest_api_init', 'wp_oembed_register_route' );

        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );

        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
endif;
add_action( 'init', 'sator_output_clean_init' );

/* https://kinsta.com/knowledgebase/disable-embeds-wordpress/ */
if ( ! function_exists( 'sator_deregister_scripts' ) ) :
    function sator_deregister_scripts() {
        wp_dequeue_script( 'wp-embed' );
    }
endif;
add_action( 'wp_footer', 'sator_deregister_scripts' );