<?php
/* Template Name: Flat */ 
?>
<?php
get_header();?>

    <div id="primary" class="site__content__primary">
        <main id="main" class="entry">

            <?php
            while ( have_posts() ) : the_post();
            ?>

            <article id="post-<?php the_ID();?>" <?php post_class('entry__post');?>>

                <div class="entry__post__content">
                    <?php
                        the_content();
                    ?>
                    
                    <section class="entry__post__content__flat">
                        <div class="h3">Voici quelques images de l’appartement…</div>
                        <?php echo do_shortcode('[DisplayGalleryPage]');?>
                    </section><!-- .entry__post__content__flat -->
                    
                </div><!-- .entry__post__content -->

                <?php if ( get_edit_post_link() ) : ?>
                    <footer class="entry__post__footer">
                        <?php
                            edit_post_link(
                                sprintf(
                                    wp_kses(
                                        __( 'Modifier <span class="screen-reader-text">%s</span>', 'sator' ),
                                        array(
                                            'span' => array(
                                                'class' => array(),
                                            ),
                                        )
                                    ),
                                    get_the_title()
                                ),
                                '<span class="entry__post__footer__edit-link">',
                                '</span>'
                            );
                        ?>
                    </footer><!-- .entry__post__footer -->
                <?php endif;?>
            </article><!-- #post-<?php the_ID();?> -->

            <?php
            endwhile;
            ?>

        </main><!-- #main .entry -->
    </div><!-- #primary .site__content__primary -->

<?php
get_sidebar();
get_footer();