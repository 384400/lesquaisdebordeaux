<?php
/**
 * Plugin Name: Sator Google Analytics
 * Plugin URI: http://www.384400.xyz
 * Description: Add Google Analytics (gtag.js) tracking code to header. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

$optionsSator = get_option( 'sator_settings' );

/**
 * Google Analytics recommande de placer le code de suivi dans l'en-tete de la page.
 */

if ( ! is_admin() ) {
    add_action( 'wp_head', 'sator_display_google_analytics_id', 1 );
}

if ( ! function_exists( 'sator_display_google_analytics_id' ) ) :
    function sator_display_google_analytics_id() {

        global $optionsSator;

        $displayAnalytics = null;

        $googleAnalyticsId = $optionsSator['idGoogleAnalytics'];

        if ( $googleAnalyticsId ) {
            $displayAnalytics = <<<EOT
<script async src="https://www.googletagmanager.com/gtag/js?id={$googleAnalyticsId}"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', '{$googleAnalyticsId}');</script>
EOT
;
            print $displayAnalytics;

        }
    }
endif;