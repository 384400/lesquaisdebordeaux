<?php
/**
 * Plugin Name: Sator Contact Form
 * Plugin URI: http://www.384400.xyz
 * Description: Display contact form [ContactForm] with Ajax and without jQuery. Sator theme is required.
 * Version: 1.0.0
 * Author: Vincent Top-Roulet
 * Author URI: http://www.384400.xyz
 * License: no license
 */

/**
 * En developpement, l'envoi de mails par l'intermediaire du serveur peut ne pas etre operationnel.
 */

$optionsSator = get_option( 'sator_settings' );

if ( $optionsSator['administratorEmail'] ) {
    $adminEmail = $optionsSator['administratorEmail'];
} else {
     $adminEmail = get_bloginfo( 'admin_email' );
}

/**
 * Nous attribuons un identifiant pour securiser l'echange du formulaire.
 */

$nonceId = 'sator-contact-form';

/**
 * Creons le formulaire.
 */


/** 
* Le W3C indique a tort "Attribute aria-required is unnecessary for elements that have attribute required.".
* Nous pourrions utiliser l'attribut "pattern", par exemple "<input id="identity" type="text" pattern="[a-zA-Z0-9_-]{6,12}" required aria-required="true" aria-label="Prenom et Nom" placeholder="Milaine Micoton" spellcheck="false" autofocus size="30" />". Cependant, les expressions regulieres incompletes produisent des resultats... hasardeux !
*/

if ( ! function_exists( 'sator_write_contact_form' ) ) :
    function sator_write_contact_form() {

        wp_enqueue_script( 'sator-recaptcha' );

        wp_enqueue_script( 'sator-contact-form-js' );

        global $optionsSator;

        global $adminEmail;

        global $nonceId;

        $nonce = wp_create_nonce( $nonceId );

        $keyCaptchaPublic = $optionsSator['keyCaptchaPublic'];

        $contentForm = <<<EOT
<div class="lang french">
    <div class="no-support-js">
        <p>Comme JavaScript est désactivé dans votre navigateur, nous vous invitons à nous écrire directement à l’adresse <a href="mailto:{$adminEmail}" title="Nous envoyer un mail">{$adminEmail}</a>.</p>
    </div><!-- .entry__post__content__form--nosupport -->
    <form id="contact" class="entry__post__content__form support-js legacy-dn">
        <p>Si vous souhaitez nous envoyer une pièce jointe, nous vous invitons à nous écrire directement à l’adresse <a href="mailto:{$adminEmail}" title="Nous envoyer un mail avec une pièce jointe">{$adminEmail}</a>.</p>
        <p><label for="identity">Identité</label></p>
        <p><input type="text" name="identity" id="identity" required aria-required="true" aria-label="Prénom et Nom" placeholder="Milaine Micoton" spellcheck="false" autofocus size="30" /></p>
        <div id="identity-check" class="entry__post__content__form__check"></div>
        <p><label for="mail">Adresse électronique</label></p>
        <p><input type="email" name="email" id="email" required aria-required="true" aria-label="Adresse électronique" placeholder="milaine-micoton@foo.bar" spellcheck="false" size="30"  /></p>
        <div id="mail-check" class="entry__post__content__form__check"></div>
        <p><label for="message">Message</label></p>
        <p><textarea name="message" id="message" required aria-required="true" aria-label="Message" spellcheck="true"></textarea></p>
        <div id="message-check" class="entry__post__content__form__check"></div>
        <p class="g-recaptcha" data-sitekey="{$keyCaptchaPublic}"></p>
        <div id="recaptcha-check" class="entry__post__content__form__check"></div>
        <p>
            <input type="hidden" name="action" value="processForm" />
            <input type="hidden" name="nonce" value="{$nonce}" />
            <input id="send" type="submit" value="Envoyer" />
        </p>
    </form><!-- #contact .entry__post__content__form support-js .legacy-dn -->
</div><!--. lang .french -->
<div id="sator-response" class="entry__post__content__form__response">
</div><!-- #sator-response .entry__post__content__form__response -->
EOT
;
        return $contentForm;

}
endif;
add_shortcode( 'ContactForm', 'sator_write_contact_form' );

/**
 * Nous traitons le formulaire avec Php.
 */

if ( ! function_exists( 'sator_process_contact_form' ) ) :

    global $optionsSator;

    $errors = [];

    function sator_process_contact_form() {

        global $nonceId, $errors;

        $response = array();

        $feedback = null;

        sleep(2);/* Pour l'esthetique. */
    
        if ( ! wp_verify_nonce( $_POST['nonce'], $nonceId ) ) {
            
            $response = array( 'feedback' => esc_html__('Une erreur a été détectée.', 'sator' ) );

            wp_send_json_error( $response );
        
        } else {
            
            $errors = [];

            array_walk_recursive( $_POST, function( &$val ) {

                $val = trim( $val );

            });

            $args = array(
                'identity'  => array(
                    'filter' => FILTER_CALLBACK,
                    'options' => 'sator_contact_check_identity'
                ),
                'email'  => array(
                    'filter' => FILTER_CALLBACK,
                    'options' => 'sator_contact_check_mail'
                ),
                'message'  => array(
                    'filter' => FILTER_CALLBACK,
                    'options' => 'sator_contact_check_message'
                ),
                'g-recaptcha-response'  => array(
                    'filter' => FILTER_CALLBACK,
                    'options' => 'sator_contact_check_recaptcha'
                )
            );

            $inputs = filter_input_array( INPUT_POST, $args );

            if ( count( $errors ) > 0 ) {
                $feedback .= esc_html__( 'Merci de corriger le formulaire.', 'sator' );
            } else {
                if ( $optionsSator['developpmentStatus'] ) {
                    $feedback .= esc_html__( 'Nous vous remercions pour votre message. Nous y donnerons suite prochainement.', 'sator' );
                } else {
                    if ( sator_contact_send_mail( $_POST['identity'], $_POST['email'], POST['message'] ) ) {
                       $feedback .= esc_html__( 'Nous vous remercions pour votre message. Nous y donnerons suite prochainement.', 'sator' );
                    } else {
                        $feedback .= esc_html__( 'Nous sommes désolés, car une erreur s’est produite. Merci de recommencer ultérieurement.', 'sator' );
                    }
                }
            }

            $response = array( 'errors' => $errors, 'feedback' => $feedback );
            /**
             * "wp_send_json_success" renvoie un objet "data".
             */
            wp_send_json_success( $response );
        }
        exit();

    }
endif;
add_action( 'wp_ajax_processForm', 'sator_process_contact_form' );
add_action( 'wp_ajax_nopriv_processForm', 'sator_process_contact_form' );

if ( ! function_exists( 'sator_contact_check_identity' ) ) :
    function sator_contact_check_identity($identity) {

        $identity = filter_var( $identity, FILTER_SANITIZE_STRING );
        if ( strlen( $identity ) >= 2 && strlen( $identity ) <= 50 ) {
            return $identity;
        }

        global $errors;

        $errors['identity'] = esc_html__( 'L’identité doit comprendre entre 2 et 50 caractères.', 'sator' );

        return false;

    }
endif;

if ( ! function_exists( 'sator_contact_check_mail' ) ) :
    function sator_contact_check_mail( $email ) {

        $email = filter_var( $email, FILTER_SANITIZE_EMAIL );

        if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
            if ( checkdnsrr( explode( '@', $email)[1] )) {
                return $email;
            }
        }

        global $errors;

        $errors['email'] = esc_html__( 'Le mail doit être sous la forme milaine-micoton@foo.bar et exister.', 'sator' );

        return false;

    }
endif;

if ( ! function_exists( 'sator_contact_check_message' ) ) :
    function sator_contact_check_message($message) {

        $message = filter_var($message, FILTER_SANITIZE_STRING);

        if ( ( strlen( $message ) >= 30 && strlen( $message ) <= 1500 ) ) {
            return $message;
        }

        global $errors;

        $errors['message'] = esc_html__( 'Le message doit comprendre entre 30 et 1500 caractères.', 'sator' );

        return false;

    }
endif;

if ( ! function_exists( 'sator_contact_check_recaptcha' ) ) :
    function sator_contact_check_recaptcha( $responseRepatcha ) {

        global $optionsSator;

        if ( ! empty( $responseRepatcha ) ) {
            /**
             * La fonction ne produit aucun resultat.
             */
            $fields = array(
                'secret' => $optionsSator['keyCaptchaPrivate'],
                'response' => $responseRepatcha,
                'remoteip' => $_SERVER['REMOTE_ADDR']
            );

            $ch = curl_init( 'https://www.google.com/recaptcha/api/siteverify' );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_TIMEOUT, 15 );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $fields ) );
            $responseGoogle = json_decode( curl_exec( $ch ) );

            if ( $responseGoogle->success ) {
                return $responseRepatcha;
            }
        }

        global $errors;

        $errors['recaptcha'] = esc_html__( 'Vous n’avez pas répondu correctement au test.', 'sator' );

        return false;

    }
endif;

if ( ! function_exists ( 'sator_contact_set_html' ) ) :
    function sator_contact_set_html() {

        return 'text/html';
    }
endif;

if ( ! function_exists( 'sator_contact_send_mail' ) ) :
    function sator_contact_send_mail( $identity, $email, $content) {

        global $optionsSator;

        global $adminEmail;

        if ( ! $optionsSator['developpmentStatus'] ) {
            /**
             * L'envoi du mail s'effectue correctement, mais le corps du message ne comprend que la lettre "p" !
             */
            add_filter( 'wp_mail_content_type', 'sator_contact_set_html' );

            $subject = '['.get_bloginfo( 'name' ).'] Nouveau message';
            
            $title = get_bloginfo( 'name' );

            $bodyMail = <<<EOT
<p>Bonjour,</p>
<p>Un message a été deposé sur $title par l'intermédiaire du formulaire de contact. Il est reproduit ci-dessous.</p>
EOT
;
            $bodyMail .= <<<EOT
<hr>
<p>$identity, $email</p>
<div>$content</div>
</hr>
EOT
;
            $bodyMail .= <<<EOT
<p>Bonne journée.</p>
<p>$title</p>
EOT
;

        if ( wp_mail ( $adminEmail, $subject, $bodyMail ) ) {

            return true;

            remove_filter( 'wp_mail_content_type', 'sator_contact_set_html' );

        }
        return false;
    }
}
endif;

/**
 * Nous gerons l'interaction du formulaire avec JavaScript.
 */

if ( ! function_exists( 'sator_script_contact_form' ) ) :
    function sator_script_contact_form() {

        global $optionsSator;

        wp_register_script( 'sator-recaptcha', 'https://www.google.com/recaptcha/api.js', '', '', true );
        
        if ( $optionsSator['developpmentStatus'] ) {
            wp_register_script( 'sator-contact-form-js', plugins_url( 'js/sator-contact-form.js', __FILE__ ), '', '', true );
        } else {
             wp_register_script( 'sator-contact-form-js', plugins_url( 'js/sator-contact-form-dist.js', __FILE__ ), '', '', true );
        }

        /* Ainsi pouvons-nous passer une chaine de caracteres qui sera traduite avec Gettext. */
        
        $toLocalize = array(
            'loadingMsg' => __( 'Chargement…', 'sator' ),
            'ajaxUrl' => admin_url( 'admin-ajax.php' )
        );

        wp_localize_script( 'sator-contact-form-js', 'ajax_objects', $toLocalize );

    }
endif;
add_action( 'wp_enqueue_scripts', 'sator_script_contact_form' );

/**
 * A propos de la validation
 * 1°. Evenement "onclick"
 * Si nous attribuons l'evenement "onclick" à "input" pour traiter le formulaire, la validation en Javascript devient inoperante... a moins de valider un à un chaque champ. Aussi gérons-nous l'evenement avec "addEventListener" pour ne pas compromettre cette validation.
 * 2°. Textes
 * A titre experimental, nous avons voulu personnaliser les messages d'alerte du champ "Email". Nous voulions passer le texte avec "wp_localize_script" puis traiter les alertes en Javascript.
 * A. Php
     $toLocalize = array(
        'messageMailEmpty' => __( 'Veuillez compléter ce champ.', 'sator' ),
        'messageMailError' => __( 'L’adresse électronique n’est pas valide.', 'sator' ) );
     wp_localize_script( 'sator-contact-form-js', 'ajax_objects', $toLocalize );
* B. Javascript
    var email = document.querySelector('#email');
    email.addEventListener('invalid', function(e) {
        if(email.validity.valueMissing) {
            e.addResponse.setCustomValidity(ajax_objects.messageMailEmpty);
        } else if(!email.validity.valid) {
            e.addResponse.setCustomValidity(ajax_objects.messageMailError);
        } 
        email.addEventListener('input', function(e){
            e.addResponse.setCustomValidity('');
        });
    });
* Or, cette methode ne fonctionne pas avec Chrome !
* A defaut, nous pourrions recourir a une declaration du texte de validation dans le champ lui-meme...
        <p><input type="email" name="email" id="email" required aria-required="true" aria-label="Adresse électronique" placeholder="milaine-micoton@foo.bar" spellcheck="false" size="30" oninvalid="this.setCustomValidity('L\'adresse électronique n\est pas valide.')" /></p>
*/
