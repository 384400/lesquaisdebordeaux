<?php
if ( ! function_exists( 'lqb_setup' ) ) :
    function lqb_setup() {
        load_child_theme_textdomain( 'lqb',  get_stylesheet_directory() . '/languages' );
    }
endif;
add_action( 'after_setup_theme', 'lqb_setup' );