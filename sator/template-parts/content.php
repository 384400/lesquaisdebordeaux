<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sator
 */

?>

<article id="post-<?php the_ID();?>" <?php post_class('entry__post');?>>
    <header class="entry__header">
        <?php
        if ( is_singular() ) :
            the_title( '<h1 class="entry__post__title">', '</h1>' );
        else :
            the_title( '<h2 class="entry__post__title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        endif;

        if ( 'post' === get_post_type() ) : ?>
        <div class="entry__post__header__meta">
            <?php sator_posted_on();?>
        </div><!-- .entry__post__header__meta -->
        <?php
        endif;?>
    </header><!-- .entry__post__header -->

    <div class="entry__post__content">
        <?php
            the_content( sprintf(
                wp_kses(
                    /* translators: %s: Name of current post. Only visible to screen readers */
                    __( 'Continuer la lecture<span class="screen-reader-text"> "%s"</span>', 'sator' ),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                get_the_title()
            ) );

            wp_link_pages( array(
                'before' => '<div class="entry__post__content__page-links">' . esc_html__( 'Pages&#x202f;:', 'sator' ),
                'after'  => '</div>',
            ) );
        ?>
    </div><!-- .entry__post__content -->

    <footer class="entry__post__footer">
        <?php sator_entry_footer();?>
    </footer><!-- .entry__post__footer -->
</article><!-- #post-<?php the_ID();?> -->